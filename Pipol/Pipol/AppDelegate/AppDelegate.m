//
//  AppDelegate.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "AppDelegate.h"
//#import "UIView+UIViewExtension.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LoginManager.h"
#import "LoginViewController.h"
#import "NSObject+NSObjectExtension.h"
#import "SWRevealViewController.h"
#import "BaseNavigationController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MyAccountViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GMSServices provideAPIKey:@"AIzaSyBjiqD2NS9PhMezwTC38EFAqGnr-gw_uPs"];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self openApp];
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)openApp {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([[LoginManager shared] isLoggedIn]) {
        MyAccountViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:[MyAccountViewController identifier]];
        BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        self.window.rootViewController = nav;

    } else {
        LoginViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:[LoginViewController identifier]];
        BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        self.window.rootViewController = nav;
    }
    [self.window makeKeyAndVisible];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}
@end
