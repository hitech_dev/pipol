//
//  RoundButton.m
//  Pipol
//
//  Created by HiTechLtd on 3/3/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "RoundButton.h"

@implementation RoundButton

- (id)init {
    self = [super init];
    if (self) {
        [self baseSetUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseSetUp];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self baseSetUp];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self baseSetUp];
}

- (void)baseSetUp {
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

@end
