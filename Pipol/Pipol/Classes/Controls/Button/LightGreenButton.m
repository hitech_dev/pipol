//
//  LightGreenButton.m
//  Pipol
//
//  Created by HiTechLtd on 3/17/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "LightGreenButton.h"

@implementation LightGreenButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    CGRect rect = [super imageRectForContentRect:contentRect];
    CGFloat y = self.frame.size.height/4.0;
    CGFloat h = self.frame.size.height/2.0;
    CGFloat w = rect.size.width * h / rect.size.height;
    CGFloat x = (self.frame.size.width - w)/2;
    return CGRectMake(x, y, w, h);
}

@end
