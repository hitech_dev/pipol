//
//  ImagePickerViewController.m
//  Pipol
//
//  Created by HiTechLtd on 4/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ImagePickerViewController.h"

@interface ImagePickerViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    UIViewController *orginialController;
    UIAlertController *alertController;
    UIImagePickerController *pickerController;
}

@end

@implementation ImagePickerViewController

-(instancetype)initAtController:(UIViewController*)controller {
    self = [super init];
    orginialController = controller;
    [self setupAlertController];
    return self;
}

#pragma mark: - list image delegate
-  (void)setupAlertController {
    if (alertController == nil) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Pipolup" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"Tomar una foto" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self actionTakePhoto];
        }];
        UIAlertAction *existPhotoAction = [UIAlertAction actionWithTitle:@"Elija existir" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self actionChooseFromGallery];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:true completion:nil];
        }];
        [alert addAction:takePhotoAction];
        [alert addAction:existPhotoAction];
        [alert addAction:cancelAction];
        alertController = alert;
    }
    [orginialController presentViewController:alertController animated:true completion:nil];
}

- (void)actionTakePhoto {
    pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [orginialController presentViewController:pickerController animated:YES completion:NULL];
    }
}

- (void)actionChooseFromGallery {
    pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [orginialController presentViewController:pickerController animated:YES completion:NULL];
    }
}

#pragma mark: - image picker delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (self.callBack && image) {
        self.callBack(image);
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
