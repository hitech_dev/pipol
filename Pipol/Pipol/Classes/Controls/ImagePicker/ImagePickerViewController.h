//
//  ImagePickerViewController.h
//  Pipol
//
//  Created by HiTechLtd on 4/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SelectedImage)(UIImage *image);

@interface ImagePickerViewController : UIViewController

-(instancetype)initAtController:(UIViewController*)controller;
//Method
@property (nonatomic, strong) SelectedImage callBack;
@end
