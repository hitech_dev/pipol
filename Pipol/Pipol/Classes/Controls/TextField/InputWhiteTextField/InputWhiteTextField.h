//
//  InputWhiteTextField.h
//  Pipol
//
//  Created by HiTechLtd on 2/28/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class InputWhiteTextField;

@protocol InputWhiteTextFieldDelegate <NSObject>
@optional
- (void)actionRightViewAtTextField:(InputWhiteTextField*)textField;

@end

@interface InputWhiteTextField : UITextField

@property (nonatomic, weak) id<InputWhiteTextFieldDelegate>inputDelegate;

- (void)showRightImage:(NSString*)imageString;
@end
