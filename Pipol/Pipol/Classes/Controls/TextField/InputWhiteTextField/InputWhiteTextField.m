//
//  InputWhiteTextField.m
//  Pipol
//
//  Created by HiTechLtd on 2/28/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "InputWhiteTextField.h"
#import <PureLayout/PureLayout.h>
#import "NSString+Help.h"

@interface InputWhiteTextField() {
    __weak UILabel *titleLabel;
    __weak CALayer *lineBottom;
    __weak UIImageView *rightImageView;
}

@end

@implementation InputWhiteTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    [self _baseSetup];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self _setupLineButom];
}

- (void)_baseSetup {
    self.borderStyle = UITextBorderStyleNone;
    self.backgroundColor = [UIColor whiteColor];
}

- (void)_setupLineButom {
    if (lineBottom == nil) {
        CALayer *_lineBottom = [[CALayer alloc] init];
        _lineBottom.frame = CGRectMake(0, self.frame.size.height - 0.8, self.frame.size.width, 0.8);
        _lineBottom.backgroundColor = [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1].CGColor;
        [self.layer addSublayer:_lineBottom];
        lineBottom = _lineBottom;
    }
}

#pragma mark: - right image
- (void)showRightImage:(NSString*)imageString {
    if (rightImageView == nil) {
        UIImageView *_rightImageView = [[UIImageView alloc] init];
        if (imageString && [imageString isEmpty]) {
            _rightImageView.image = [UIImage imageNamed:imageString];
        } else {
            _rightImageView.image = [UIImage imageNamed:@"ic_help.png"];
        }
        _rightImageView.contentMode = UIViewContentModeScaleAspectFill;
        _rightImageView.clipsToBounds = YES;
        self.rightViewMode = UITextFieldViewModeAlways;
        self.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height - 12, self.frame.size.height - 12)];
        [self.rightView addSubview:_rightImageView];
        _rightImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [_rightImageView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        rightImageView = _rightImageView;
        
        //add button
        UIButton *_button = [[UIButton alloc] init];
        [self.rightView addSubview:_button];
        _button.translatesAutoresizingMaskIntoConstraints = NO;
        [_button autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        [_button addTarget:self action:@selector(actionRightView:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)actionRightView:(UIButton*)sender {
    if ([self.inputDelegate respondsToSelector:@selector(actionRightViewAtTextField:)]) {
        [self.inputDelegate actionRightViewAtTextField:self];
    }
}

@end
