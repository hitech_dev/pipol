//
//  LeftPaddingTextField.m
//  Pipol
//
//  Created by HiTechLtd on 3/27/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "LeftPaddingTextField.h"

@implementation LeftPaddingTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 10)];
    [self setLeftView: paddingView];
    self.leftViewMode = UITextFieldViewModeAlways;
}

@end
