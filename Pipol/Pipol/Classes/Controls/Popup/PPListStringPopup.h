//
//  PPListStringPopup.h
//  Pipol
//
//  Created by HiTechLtd on 3/30/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "BaseViewController.h"

typedef void (^PopupHandler)(id object, NSInteger indexSelectedProvince, NSArray *provinceArray, NSArray *cityArray);

@interface PPListStringPopup : BaseViewController

@property (nonatomic, strong) PopupHandler handler;
@property (nonatomic, assign) NSInteger indexSeletedProvince;
@property (nonatomic, strong) NSArray *provinceArray;
@property (nonatomic, strong) NSArray *cityArray;

@end
