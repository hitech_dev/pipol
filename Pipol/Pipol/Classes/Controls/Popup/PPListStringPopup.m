//
//  PPListStringPopup.m
//  Pipol
//
//  Created by HiTechLtd on 3/30/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPListStringPopup.h"
#import "NSString+Help.h"
#import "BaseNavigationController.h"
#import "UIHelpers.h"
#import "PPCityManager.h"
#import "PPCityModel.h"
#import "PPProvinceModel.h"
#import "PPCountryModel.h"
#import "AppDefine.h"

@interface PPListStringPopup()<UITableViewDataSource, UITableViewDelegate> {
    IBOutlet __weak UITableView *_tableView;
    BOOL isSelectedProvince;
    NSInteger provinceID;
    NSInteger _indexSelected;
}
@property (nonatomic, strong) NSArray *data;
@end

@implementation PPListStringPopup

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTableView];
    [self setupBackButton];
    self.title = @"Seleccionar la ciudad";
    isSelectedProvince = NO;
    provinceID = 0;
}

- (void)setupBackButton {
    UIImage *buttonImage = [UIImage imageNamed:@"ic_nav_back.png"];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, 12, 10);
    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(__popVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:aBarButtonItem];
}

#pragma mark: - actions
- (void)__popVC:(UIButton*)sender {
    if (isSelectedProvince == YES) {
        if (self.provinceArray == nil) {
            [self callAPIGetProvince];
        } else {
            self.data = self.provinceArray;
            _indexSelected = self.indexSeletedProvince;
            [_tableView reloadData];
        }
        isSelectedProvince = NO;
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)setUpTableView {
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"Cell"];
    _tableView.separatorInset = UIEdgeInsetsZero;
    if (self.provinceArray == nil) {
        [self callAPIGetProvince];
    } else {
        self.data = self.provinceArray;
        _indexSelected = self.indexSeletedProvince;
    }
    
}

- (void)actionSave:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark: - table datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.data != nil) {
        return self.data.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (self.data != nil && self.data.count > indexPath.row) {
        NSDictionary *item = self.data[indexPath.row];
        cell.textLabel.text = item[@"name"];
    }
    if (indexPath.row == _indexSelected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.preservesSuperviewLayoutMargins = NO;
    cell.layoutMargins = UIEdgeInsetsZero;
    return cell;
}

#pragma mark: - tableview delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70 * ScreenScale;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    //select city
    if (isSelectedProvince == YES) {
        if (self.handler) {
            if (self.data != nil && self.data.count > indexPath.row) {
                NSDictionary *item = self.data[indexPath.row];
                self.handler(item, self.indexSeletedProvince, self.provinceArray, self.cityArray);
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //select province
        self.indexSeletedProvince = indexPath.row;
        NSDictionary *province = [self.data objectAtIndex:indexPath.row];
        if (province) {
            NSInteger provinID = [[province objectForKey:@"id"] integerValue];
            self.cityArray = nil;
            if (self.cityArray == nil) {
                [self callAPIGetCityWithProvinceID:provinID];
            } else {
                self.data = self.cityArray;
                [tableView reloadData];
            }
            
            isSelectedProvince = YES;
        }
    }

}

- (void)tableView:(UITableView*)tableView refreshStatusCellWhenSelectAtIndexPath:(NSIndexPath*)indexPath {
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow: _indexSelected inSection:0];
    UITableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UITableViewCell *oldCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:oldIndexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    oldCell.accessoryType = UITableViewCellAccessoryNone;
    [tableView reloadRowsAtIndexPaths:@[indexPath, oldIndexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark: - API

- (void)callAPIGetProvince {
    [UIHelpers showLoadingView:kWindow];
    [[PPCityManager shared] getListProvinceWithID:nil countryID:nil complition:^(NSArray *result, NSString *error) {
        [UIHelpers hideLoadingView];
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        } else {
            self.provinceArray = [self arrayProvinceAddressFromProvinceModelArray:result];
            self.data = self.provinceArray;
            [kMainQueue addOperationWithBlock:^{
                [_tableView reloadData];
            }];
        }
    }];
}

- (void)callAPIGetCityWithProvinceID:(NSInteger)provinceId {
    [UIHelpers showLoadingView:kWindow];
    [[PPCityManager shared] getListCityWithID:nil provinceID: [NSNumber numberWithInteger: provinceId] complition:^(NSArray *result, NSString *error) {
        [UIHelpers hideLoadingView];
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        } else {
            self.cityArray = [self arrayCityAddressFromCityModelArray:result];
            self.data = self.cityArray;
            [kMainQueue addOperationWithBlock:^{
                [_tableView reloadData];
            }];
        }
    }];
}

- (NSArray*)arrayCityAddressFromCityModelArray:(NSArray*)array {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
    for (PPCityModel *model in array) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item setObject:[NSNumber numberWithInt:model.cityId] forKey:@"id"];
        [item setObject:model.cityName forKey:@"name"];
        [result addObject:item];
    }
    return result;
}

- (NSArray*)arrayProvinceAddressFromProvinceModelArray:(NSArray*)array {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
    for (PPProvinceModel *model in array) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item setObject:[NSNumber numberWithInt:model.provinceId] forKey:@"id"];
        [item setObject:model.provinceName forKey:@"name"];
        [result addObject:item];
    }
    return result;
}

- (NSArray*)arrayCountryAddressFromCountryModelArray:(NSArray*)array {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
    for (PPCityModel *model in array) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item setObject:[NSNumber numberWithInt:model.cityId] forKey:@"id"];
        [item setObject:model.cityName forKey:@"name"];
        [result addObject:item];
    }
    return result;
}
@end
