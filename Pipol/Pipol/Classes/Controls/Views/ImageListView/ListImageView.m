//
//  ListImageView.m
//  Pipol
//
//  Created by HiTechLtd on 3/19/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ListImageView.h"
#import "UIHelpers.h"
#import "AppDefine.h"

typedef NS_ENUM(NSInteger, ListImageMode) {
    ListImageSelect,
    ListImageDelete,
};

@interface ListImageView()<UIActionSheetDelegate> {
    __weak IBOutlet UIImageView *imageView1;
    __weak IBOutlet UIImageView *imageView2;
    __weak IBOutlet UIImageView *imageView3;
    
    __weak IBOutlet UIImageView *deleteImageView1;
    __weak IBOutlet UIImageView *deleteImageView2;
    __weak IBOutlet UIImageView *deleteImageView3;
    BOOL isCircling;
}

@end

@implementation ListImageView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (isCircling != true) {
        [deleteImageView1.layer setCornerRadius:deleteImageView1.frame.size.height/2];
        [deleteImageView2.layer setCornerRadius:deleteImageView2.frame.size.height/2];
        [deleteImageView3.layer setCornerRadius:deleteImageView3.frame.size.height/2];
        isCircling = true;
    }
}

- (void)setupUI {
    UITapGestureRecognizer *tapSelect1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSelectImage1)];
    UITapGestureRecognizer *tapSelect2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSelectImage2)];
    UITapGestureRecognizer *tapSelect3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSelectImage3)];
    
    UITapGestureRecognizer *tapDelete1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDeleteImage1)];
    UITapGestureRecognizer *tapDelete2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDeleteImage2)];
    UITapGestureRecognizer *tapDelete3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDeleteImage3)];
    
    [imageView1 addGestureRecognizer:tapSelect1];
    [imageView2 addGestureRecognizer:tapSelect2];
    [imageView3 addGestureRecognizer:tapSelect3];
    
    [deleteImageView1 addGestureRecognizer:tapDelete1];
    [deleteImageView2 addGestureRecognizer:tapDelete2];
    [deleteImageView3 addGestureRecognizer:tapDelete3];
}

#pragma mark: - action select image
- (void)actionSelectImage1 {
    [self selectImageAtIndex:1];
}
- (void)actionSelectImage2 {
    [self selectImageAtIndex:2];
}
- (void)actionSelectImage3 {
    [self selectImageAtIndex:3];
}

#pragma mark: - action delete image
- (void)actionDeleteImage1 {
    [self refreshImageAtIndex:1 withImage:nil withMode:ListImageDelete];
}
- (void)actionDeleteImage2 {
    [self refreshImageAtIndex:2 withImage:nil withMode:ListImageDelete];
}
- (void)actionDeleteImage3 {
    [self refreshImageAtIndex:3 withImage:nil withMode:ListImageDelete];
}

#pragma mark: - select Image
- (void)selectImageAtIndex:(int)index {
    if ([self.delegate respondsToSelector:@selector(selectImageAtIndex:)]) {
       [self.delegate selectImageAtIndex:index];
    }
}



- (void)refreshImage:(UIImage*)image atIndex:(int)index {
    if (image) {
        [self refreshImageAtIndex:index withImage:image withMode:ListImageSelect];
    }
}

- (void)refreshImageAtIndex:(int)index withImage:(UIImage*)image withMode:(ListImageMode)mode {
    if (mode == ListImageSelect) {
        [kMainQueue addOperationWithBlock:^{
            UIImage *deleteImage = [UIImage imageNamed:@"ic_delete.png"];
            if (index == 1) {
                imageView1.image = image;
                deleteImageView1.image = deleteImage;
            } else if (index == 2) {
                imageView2.image = image;
                deleteImageView2.image = deleteImage;
            } else {
                imageView3.image = image;
                deleteImageView3.image = deleteImage;
            }
        }];
    } else if (mode == ListImageDelete) {
        [kMainQueue addOperationWithBlock:^{
            UIImage *placeholderImage = [UIImage imageNamed:@"img_placeholder_list_image.png"];
            if (index == 1) {
                imageView1.image = placeholderImage;
                deleteImageView1.image = nil;
            } else if (index == 2) {
                imageView2.image = placeholderImage;
                deleteImageView2.image = nil;
            } else {
                imageView3.image = placeholderImage;
                deleteImageView3.image = nil;
            }
        }];
    }
    if ([self.delegate respondsToSelector:@selector(removeImageAtIndex:)]) {
        [self.delegate removeImageAtIndex:index];
    }
}
@end
