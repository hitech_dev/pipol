//
//  ListImageView.h
//  Pipol
//
//  Created by HiTechLtd on 3/19/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ListImageView;

@protocol ListImageViewDelegate <NSObject>
@optional
-  (void)selectImageAtIndex:(int)index;
-  (void)removeImageAtIndex:(int)index;

@end

@interface ListImageView : UIView

- (void)refreshImage:(UIImage*)image atIndex:(int)index;
@property (nonatomic, weak) id<ListImageViewDelegate>delegate;
@end
