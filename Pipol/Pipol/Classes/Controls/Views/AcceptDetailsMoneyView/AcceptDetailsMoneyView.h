//
//  AcceptDetailsMoneyView.h
//  Pipol
//
//  Created by HiTechLtd on 2/28/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  AcceptDetailsMoneyView;

@protocol AcceptDetailsMoneyViewDelegate <NSObject>
@optional
- (void)view:(AcceptDetailsMoneyView*)view acionAccept:(UIButton*)sender;

@end

@interface AcceptDetailsMoneyView : UIView

@property (nonatomic, weak) IBOutlet UIButton *btnAccept;
@property (nonatomic, weak) id<AcceptDetailsMoneyViewDelegate>delegate;

+ (AcceptDetailsMoneyView*)nibView;
@end
