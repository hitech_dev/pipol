//
//  AcceptDetailsMoneyView.m
//  Pipol
//
//  Created by HiTechLtd on 2/28/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "AcceptDetailsMoneyView.h"
#import "UIView+UIViewExtension.h"

@implementation AcceptDetailsMoneyView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btnAccept.layer.cornerRadius = _btnAccept.frame.size.height/2;
}

+ (AcceptDetailsMoneyView*)nibView {
    AcceptDetailsMoneyView *view = [[NSBundle mainBundle] loadNibNamed:[AcceptDetailsMoneyView identifier] owner:nil options:nil][0];
    return view;
}

- (IBAction)__actionAccept:(id)sender {
    if ([self.delegate respondsToSelector:@selector(view:acionAccept:)]) {
        [self.delegate view:self acionAccept:sender];
    }
}
@end
