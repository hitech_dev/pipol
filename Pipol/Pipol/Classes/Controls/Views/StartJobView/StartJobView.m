//
//  StartJobView.m
//  Pipol
//
//  Created by HiTechLtd on 2/29/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "StartJobView.h"
#import "UIView+UIViewExtension.h"
#import "PPLocationObject.h"
#import <GoogleMaps/GoogleMaps.h>

@interface StartJobView() {
    int _timeCount;
    
    __weak IBOutlet UIButton *btnStart;
    __weak IBOutlet UIButton *btnProblem;
    __weak IBOutlet UILabel *lblTimer;
    
}

@end

@implementation StartJobView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self _startTimer];
    [self _setupButtons];
}

+ (StartJobView*)nibView {
    StartJobView *view = [[NSBundle mainBundle] loadNibNamed:[StartJobView identifier] owner:nil options:nil][0];
    return view;
}

- (void)_setupButtons {
    btnStart.layer.cornerRadius = btnStart.frame.size.height/2;
    [btnStart setEnabled:NO];
    btnStart.alpha = 0.5;
    btnProblem.layer.cornerRadius = btnStart.frame.size.height/2;
}
#pragma mark: - timer
- (void)_startTimer {
    _timeCount = 1800;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(_refreshTime) userInfo:nil repeats:YES];
}

#pragma mark: refreshs
- (void)_refreshTime {
    _timeCount--;
    if (_timeCount == 0) {
        [self.timer invalidate];
        self.timer = nil;
        [self didFinishTimer];
    } else {
        [self _refreshTimeLabel];
    }
}

- (void)_refreshTimeLabel {
    int minute = _timeCount / 60;
    int second = _timeCount % 60;
    NSString *strMinute;
    NSString *strSecond;
    if (minute < 10) {
        strMinute = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", minute]];
    } else {
        strMinute = [NSString stringWithFormat:@"%d", minute];
    }
    if (second < 10) {
        strSecond = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", second]];
    } else {
        strSecond = [NSString stringWithFormat:@"%d", second];
    }
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        lblTimer.text = [[[@"00 : " stringByAppendingString:strMinute] stringByAppendingString:@"."] stringByAppendingString:strSecond];
    }];
    [self refreshStartButton];
}

- (void)refreshStartButton {
    CLLocation *mylocation = self.mapView.myLocation;
//    CLLocation *mylocation = [[CLLocation alloc] initWithLatitude:self.jobLocation.coordinate.latitude longitude:self.jobLocation.coordinate.longitude ];
    if (mylocation && self.jobLocation) {
        CLLocationDistance distance = [self.jobLocation distanceFromLocation:mylocation];
        NSLog(@"Distan: %f",  distance);
        if (distance <= 50) {
            [btnStart setEnabled:YES];
            btnStart.alpha = 1;
        }
    }
}

- (void)didFinishTimer {
    if ([self.delegate respondsToSelector:@selector(startJobviewDidFinishTimer:)]) {
        [self.delegate startJobviewDidFinishTimer:self];
    }
}
#pragma mark: actions
- (IBAction)__actionStart:(id)sender {
    if ([self.delegate respondsToSelector:@selector(startJobview:acionStart:)]) {
        [self.delegate startJobview:self acionStart:sender];
    }
}

- (IBAction)__actionProblem:(id)sender {
    if ([self.delegate respondsToSelector:@selector(startJobview:acionProblem:)]) {
        [self.delegate startJobview:self acionProblem:sender];
    }
}
@end
