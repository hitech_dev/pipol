//
//  StartJobView.h
//  Pipol
//
//  Created by HiTechLtd on 2/29/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@class  StartJobView;
@class GMSMapView;

@protocol StartJobViewDelegate <NSObject>
@optional
- (void)startJobview:(StartJobView*)view acionStart:(UIButton*)sender;
- (void)startJobview:(StartJobView*)view acionProblem:(UIButton*)sender;
- (void)startJobviewDidFinishTimer:(StartJobView*)view;
@end

@interface StartJobView : UIView

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) CLLocation *jobLocation;
@property (nonatomic, strong) GMSMapView *mapView;

@property (nonatomic, weak) id<StartJobViewDelegate>delegate;
+ (StartJobView*)nibView;
@end
