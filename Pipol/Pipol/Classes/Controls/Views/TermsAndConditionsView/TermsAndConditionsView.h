//
//  TermsAndConditionsView.h
//  Pipol
//
//  Created by HiTechLtd on 4/14/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol TermsAndConditionsViewDelegate <NSObject>
@optional
- (void)acceptedTerms;
@end

@interface TermsAndConditionsView : UIView

@property (nonatomic, weak) id<TermsAndConditionsViewDelegate>delegate;


+ (TermsAndConditionsView *)nibView;
@end
