//
//  TermsAndConditionsView.m
//  Pipol
//
//  Created by HiTechLtd on 4/14/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "TermsAndConditionsView.h"
#import "UIView+UIViewExtension.h"

@interface TermsAndConditionsView()
{
    __weak IBOutlet UIView *termsView;
    __weak IBOutlet UITextView *txvContent;
}

@end

@implementation TermsAndConditionsView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupUI];
}

+ (TermsAndConditionsView*)nibView {
    TermsAndConditionsView *view = [[NSBundle mainBundle] loadNibNamed:[TermsAndConditionsView identifier] owner:nil options:nil][0];
    return view;
}

- (void)setupUI {
    [txvContent setContentOffset:CGPointZero];
    termsView.layer.cornerRadius = 10.0f;
    termsView.clipsToBounds = YES;
}


#pragma mark - actions
- (IBAction)__didTapAcceptedTerms:(id)sender {
    [self.delegate acceptedTerms];
}


@end
