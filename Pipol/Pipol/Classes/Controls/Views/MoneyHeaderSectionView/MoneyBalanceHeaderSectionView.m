//
//  MoneyBalanceHeaderSectionView.m
//  Pipol
//
//  Created by HiTechLtd on 2/27/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MoneyBalanceHeaderSectionView.h"
#import "UIView+UIViewExtension.h"

@implementation MoneyBalanceHeaderSectionView

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (MoneyBalanceHeaderSectionView*)nibView {
    MoneyBalanceHeaderSectionView *view = [[NSBundle mainBundle] loadNibNamed:[MoneyBalanceHeaderSectionView identifier] owner:nil options:nil][0];
    return view;
}


@end
