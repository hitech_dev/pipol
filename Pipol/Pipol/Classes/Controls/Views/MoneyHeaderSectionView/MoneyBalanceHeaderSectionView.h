//
//  MoneyBalanceHeaderSectionView.h
//  Pipol
//
//  Created by HiTechLtd on 2/27/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoneyBalanceHeaderSectionView : UIView

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblValue;

+ (MoneyBalanceHeaderSectionView*)nibView;
@end
