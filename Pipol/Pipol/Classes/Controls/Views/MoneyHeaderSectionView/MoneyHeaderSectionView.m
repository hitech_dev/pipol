//
//  MoneyHeaderSectionView.m
//  Pipol
//
//  Created by HiTechLtd on 2/27/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MoneyHeaderSectionView.h"
#import "UIView+UIViewExtension.h"

@implementation MoneyHeaderSectionView

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (MoneyHeaderSectionView*)nibView {
    MoneyHeaderSectionView *view = [[NSBundle mainBundle] loadNibNamed:[MoneyHeaderSectionView identifier] owner:nil options:nil][0];
    return view;
}

@end
