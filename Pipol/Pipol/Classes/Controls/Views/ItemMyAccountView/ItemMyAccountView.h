//
//  ItemMyAccountView.h
//  Pipol
//
//  Created by HiTechLtd on 2/25/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemMyAccountView : UIView

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblContent;

- (void)titleWithText:(NSString*)text;
- (void)contentWithText:(NSString*)text;
@end
