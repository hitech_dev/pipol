//
//  ItemMyAccountView.m
//  Pipol
//
//  Created by HiTechLtd on 2/25/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ItemMyAccountView.h"

@implementation ItemMyAccountView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self _setupLayer];
}

- (void)_setupLayer {
    self.layer.borderColor = [[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1] CGColor];
    self.layer.borderWidth = 1.0;
    self.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
}

- (void)titleWithText:(NSString*)text {
    self.lblTitle.text = text;
}

- (void)contentWithText:(NSString*)text {
    self.lblContent.text = text;
}

@synthesize lblTitle = _lblTitle;
@end
