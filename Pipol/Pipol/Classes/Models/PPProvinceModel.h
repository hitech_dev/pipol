//
//  PPProvinceModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
@class PPCountryModel;

@interface PPProvinceModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int provinceId;
@property (nonatomic, strong) NSString *provinceName;
@property (nonatomic, strong) PPCountryModel *provinceCountry;
@end
