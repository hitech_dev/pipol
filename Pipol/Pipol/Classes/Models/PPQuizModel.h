//
//  PPQuizModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/10/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PPQuizModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int quizId;
@property (nonatomic, strong) NSString *quizContent;
@property (nonatomic, strong) NSString *quizAnswer;
@end
