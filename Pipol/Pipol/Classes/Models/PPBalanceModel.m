//
//  PPBalanceModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPBalanceModel.h"
#import "PPJobModel.h"

@implementation PPBalanceModel

+(NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"balanceId"   : @"Id",
             @"balanceType" : @"Type",
             @"balanceDecimal" : @"Amount",
             @"balanceJob" : @"Job"
             };
}

+ (NSValueTransformer *)balanceJobJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass: [PPJobModel class]];
}

@end
