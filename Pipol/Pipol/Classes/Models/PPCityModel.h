//
//  PPCityModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "PPProvinceModel.h"

@interface PPCityModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int cityId;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) PPProvinceModel *cityProvince;
@end
