//
//  PPCompanyModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PPCompanyModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int companyId;
@property (nonatomic, strong) NSString *companyBusinessName;
@property (nonatomic, strong) NSString *companyPhone;
@property (nonatomic, strong) NSString *companyAddress;
@end
