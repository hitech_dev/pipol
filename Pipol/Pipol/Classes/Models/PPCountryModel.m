//
//  PPCountryModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPCountryModel.h"

@implementation PPCountryModel

+(NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"countryId"   : @"Id",
             @"countryName" : @"Name"
             };
}

@end
