//
//  PPUserModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPUserModel.h"
#import <Mantle/Mantle.h>
#import "UIHelpers.h"
#import "PPCityModel.h"

@implementation PPUserModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"userId" : @"Id",
             @"userName" : @"Name",
             @"userLastname" : @"Lastname",
             @"userImage" : @"Image",
             @"userEmail" : @"Email",
             @"userPhone" : @"Phone",
             @"userAddress" : @"Address",
             @"userGender" : @"Gender",
             @"userCity" : @"City",
             @"userBirthDate" : @"BirthDate",
             @"userDNI" : @"DNI",
             @"userFacebookId" : @"FacebookId"
             };
}

+(NSValueTransformer*)userBirthDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)userCityJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass: [PPCityModel class]];
}



@end
