//
//  PPProvinceModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPProvinceModel.h"
#import "PPCountryModel.h"

@implementation PPProvinceModel

+(NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"provinceId"   : @"Id",
             @"provinceName" : @"Name",
             @"provinceCountry" : @"Country"
             };
}

+ (NSValueTransformer *)provinceCountryJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass: [PPCountryModel class]];
}

@end
