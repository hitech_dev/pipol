//
//  PPQuizModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/10/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPQuizModel.h"

@implementation PPQuizModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"quizId" : @"Id",
             @"quizContent" : @"Content",
             @"quizAnswer" : @"Answer"
             };
}

@end
