//
//  PPJobModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPJobModel.h"
#import "PPJobBranchModel.h"
#import "PPUserModel.h"
#import "PPQuizModel.h"
#import "PPImageModel.h"
#import "PPAudioModel.h"
#import "UIHelpers.h"

@implementation PPJobModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
        @"jobId" : @"Id",
        @"jobTitle" : @"Title",
        @"jobDescription" : @"Description",
        @"jobDueDate" : @"DueDate",
        @"jobBranch" : @"Branch",
        @"jobUser" : @"User",
        @"jobApplyDate" : @"ApplyDate",
        
        @"jobFinishDate" : @"FinishDate",
        @"jobImage" : @"Image",
        @"jobMaxTime" : @"MaxTime",
        @"jobPayAmount" : @"PayAmount",
        
        @"jobFinished" : @"Finished",
        @"jobQuizzes" : @"Quizzes",
        @"jobImages" : @"Images",
        @"jobAudios" : @"Audios"
    };
}

+ (NSValueTransformer *)jobDueDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)jobBranchJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass: [PPJobBranchModel class]];
}

+ (NSValueTransformer *)jobUserJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass: [PPUserModel class]];
}

+ (NSValueTransformer *)jobApplyDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)jobFinishDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [UIHelpers.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)jobQuizzesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass: [PPQuizModel class]];
}

+ (NSValueTransformer *)jobImagesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass: [PPImageModel class]];
}

+ (NSValueTransformer *)jobAudiosJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass: [PPAudioModel class]];
}
@end
