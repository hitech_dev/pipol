//
//  PPCityModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPCityModel.h"

@implementation PPCityModel

+(NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"cityId"   : @"Id",
             @"cityName" : @"Name",
             @"cityProvince" : @"Province"
             };
}

+ (NSValueTransformer *)cityProvinceJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass: [PPProvinceModel class]];
}

@end
