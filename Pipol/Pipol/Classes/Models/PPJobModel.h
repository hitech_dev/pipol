//
//  PPJobModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Mantle/Mantle.h>
@class PPJobBranchModel;
@class PPUserModel;

@interface PPJobModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int jobId;
@property (nonatomic, strong) NSString *jobTitle;
@property (nonatomic, strong) NSString *jobDescription;
@property (nonatomic, strong) NSDate *jobDueDate;
@property (nonatomic, strong) PPJobBranchModel *jobBranch;
@property (nonatomic, strong) PPUserModel *jobUser;
@property (nonatomic, strong) NSDate *jobApplyDate;
@property (nonatomic, strong) NSDate *jobFinishDate;
@property (nonatomic, strong) NSString *jobImage;
@property (nonatomic, assign) int jobMaxTime;
@property (nonatomic, assign) int jobPayAmount;
@property (nonatomic, assign) BOOL jobFinished;
@property (nonatomic, strong) NSArray *jobQuizzes;
@property (nonatomic, strong) NSArray *jobImages;
@property (nonatomic, strong) NSArray *jobAudios;
@end
