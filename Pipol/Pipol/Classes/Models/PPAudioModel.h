//
//  PPAudioModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/17/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface PPAudioModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int imgID;
@property (nonatomic, strong) NSString *imgName;
@property (nonatomic, strong) NSString *imgFile;
@end
