//
//  PPUserModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Mantle/Mantle.h>
@class PPCityModel;

@interface PPUserModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userLastname;
@property (nonatomic, strong) NSString *userImage;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userPhone;
@property (nonatomic, strong) NSString *userAddress;
@property (nonatomic, assign) int userGender;
@property (nonatomic, strong) PPCityModel *userCity;
@property (nonatomic, strong) NSDate *userBirthDate;
@property (nonatomic, assign) int userDNI;
@property (nonatomic, assign) long userFacebookId;
@end
