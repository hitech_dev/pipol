//
//  PPJobBranchModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPJobBranchModel.h"
#import "PPCompanyModel.h"

@implementation PPJobBranchModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"branchId" : @"Id",
             @"branchName" : @"Name",
             @"branchAddress" : @"Address",
             @"branchPhone" : @"Phone",
             @"branchCompany" : @"Company",
             @"branchLatitude" : @"Latitude",
             @"branchLongitude" : @"Longitude",
             };
}

+ (NSValueTransformer *)branchCompanyJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PPCompanyModel class]];
}

@end
