
//
//  PPImageModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/17/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPImageModel.h"

@implementation PPImageModel

+(NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"imgID"   : @"Id",
             @"imgName" : @"Name",
             @"imgFile" : @"File"
             };
}

@end
