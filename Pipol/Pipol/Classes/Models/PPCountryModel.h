//
//  PPCountryModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface PPCountryModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int countryId;
@property (nonatomic, strong) NSString *countryName;
@end
