//
//  PPBalanceModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
@class PPJobModel;

@interface PPBalanceModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int balanceId;
@property (nonatomic, assign) int balanceType;
@property (nonatomic, assign) int balanceDecimal;
@property (nonatomic, strong) PPJobModel *balanceJob;
@end
