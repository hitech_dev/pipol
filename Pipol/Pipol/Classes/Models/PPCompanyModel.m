//
//  PPCompanyModel.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPCompanyModel.h"


@implementation PPCompanyModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
        @"companyId"            : @"Id",
        @"companyBusinessName"  : @"BusinessName",
        @"companyPhone"         : @"Phone",
        @"companyAddress"       : @"Address"
    };
}

@end
