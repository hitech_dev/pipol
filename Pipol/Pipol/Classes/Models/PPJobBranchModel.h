//
//  PPJobBranchModel.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Mantle/Mantle.h>
@class PPCompanyModel;

@interface PPJobBranchModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) int branchId;
@property (nonatomic, strong) NSString *branchName;
@property (nonatomic, strong) NSString *branchAddress;
@property (nonatomic, strong) NSString *branchPhone;
@property (nonatomic, strong) PPCompanyModel *branchCompany;
@property (nonatomic, strong) NSNumber *branchLatitude;
@property (nonatomic, strong) NSNumber *branchLongitude;
@end