//
//  PPLocationObject.m
//  Pipol
//
//  Created by HiTechLtd on 3/19/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPLocationObject.h"

@implementation PPLocationObject

- (id)initLatitude:(CLLocationDistance)latitude Longitude:(CLLocationDistance)longitude {
    self = [super init];
    if (self) {
        self.latitude = latitude;
        self.longitude = longitude;
    }
    return self;
}
@end
