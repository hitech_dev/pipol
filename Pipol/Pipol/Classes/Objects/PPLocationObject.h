//
//  PPLocationObject.h
//  Pipol
//
//  Created by HiTechLtd on 3/19/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PPLocationObject : NSObject

@property (nonatomic, assign) CLLocationDistance latitude;
@property (nonatomic, assign) CLLocationDistance longitude;

- (id)initLatitude:(CLLocationDistance)latitude Longitude:(CLLocationDistance)longitude;
@end
