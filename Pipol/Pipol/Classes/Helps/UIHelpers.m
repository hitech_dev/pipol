//
//  UIHelpers.m
//  Pipol
//
//  Created by HiTechLtd on 3/14/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "UIHelpers.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AppDefine.h"

@implementation UIHelpers

+ (void)showLoadingView:(UIView*)view {
    //add translucent view
    [kMainQueue addOperationWithBlock:^{
        [SVProgressHUD show];
        [SVProgressHUD setViewForExtension:view];
        [SVProgressHUD setStatus:@"Loading..."];
    }];
}

+ (void)hideLoadingView {
    //remove translucent view
    [kMainQueue addOperationWithBlock:^{
        [SVProgressHUD dismiss];
    }];
}

+ (void)showAlertViewErrorWithMessage:(NSString*)message delegate:(id)delegate tag:(NSInteger)tag {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:m_string(@"Pipolup")
                                                       message: message
                                                      delegate: delegate
                                             cancelButtonTitle:m_string(@"OK")
                                             otherButtonTitles: nil];
        alert.tag = tag;
        [alert show];
    }];
}

+ (void)showAlertViewErrorWithMessage:(NSString*)message cancelButton:(NSString*)cancelTitle delegate:(id)delegate tag:(NSInteger)tag {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:m_string(@"Pipolup")
                                                       message: message
                                                      delegate: delegate
                                             cancelButtonTitle:cancelTitle
                                             otherButtonTitles: m_string(@"OK"), nil];
        alert.tag = tag;
        [alert show];
    }];
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    return dateFormatter;
}

+ (NSString*)stringTimeFromDate:(NSDate*)dateFrom intoDate:(NSDate*)dateStill {
    NSTimeInterval intervalRemain = [dateStill timeIntervalSince1970] - [dateFrom timeIntervalSince1970];
    int _timeCount = (int)intervalRemain;
    if (_timeCount >= 0) {
        int hour = _timeCount / 3600;
        int minute = (_timeCount %  3600) / 60;
        int second = (_timeCount %  3600) % 60;
        NSString *strHour;
        NSString *strMinute;
        NSString *strSecond;
        if (hour < 10) {
            strHour = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", hour]];
        } else {
            strHour = [NSString stringWithFormat:@"%d", hour];
        }
        if (minute < 10) {
            strMinute = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", minute]];
        } else {
            strMinute = [NSString stringWithFormat:@"%d", minute];
        }
        if (second < 10) {
            strSecond = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", second]];
        } else {
            strSecond = [NSString stringWithFormat:@"%d", second];
        }
        NSString *result = [[[[strHour stringByAppendingString:@" : "] stringByAppendingString:strMinute] stringByAppendingString:@"."] stringByAppendingString:strSecond];
        return result;
    }
    return @"Expirar";
}

+ (UIDatePicker*)datePicker {
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.timeZone = [NSTimeZone defaultTimeZone];
    datePicker.minuteInterval = 5;
    datePicker.datePickerMode = UIDatePickerModeDate;
    return datePicker;
}

@end
