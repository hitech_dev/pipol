//
//  DummyData.h
//  Pipol
//
//  Created by HiTechLtd on 3/11/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DummyData : NSObject

+(instancetype)shared;

- (NSArray*)jobsArray;
@end
