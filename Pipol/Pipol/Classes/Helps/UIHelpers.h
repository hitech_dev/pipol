//
//  UIHelpers.h
//  Pipol
//
//  Created by HiTechLtd on 3/14/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIHelpers : NSObject

+ (void)showLoadingView:(UIView*)view;
+ (void)hideLoadingView;
+ (void)showAlertViewErrorWithMessage:(NSString*)message delegate:(id)delegate tag:(NSInteger)tag;
+ (void)showAlertViewErrorWithMessage:(NSString*)message cancelButton:(NSString*)cancelTitle delegate:(id)delegate tag:(NSInteger)tag;
+ (NSDateFormatter *)dateFormatter;
+ (NSString*)stringTimeFromDate:(NSDate*)dateFrom intoDate:(NSDate*)dateStill;
+ (UIDatePicker*)datePicker;
@end
