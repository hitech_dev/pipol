//
//  DummyData.m
//  Pipol
//
//  Created by HiTechLtd on 3/11/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "DummyData.h"
#import "PPJobModel.h"

@implementation DummyData

+(instancetype)shared {
    static DummyData *instance = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSArray*)jobsArray {
    PPJobModel *model = [PPJobModel new];
    return @[model,model,model,model,model,model];
}

@end
