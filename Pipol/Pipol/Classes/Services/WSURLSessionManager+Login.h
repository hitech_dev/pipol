//
//  WSURLSessionManager+Login.h
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager.h"
@class WSRequest;

@interface WSURLSessionManager (Login)

- (void)wsLoginWithRequest:(WSRequest *)request handler:(ServiceTokenHandler)handler;
- (void)wsRegisterWithRequest:(WSRequest *)request handler:(ServiceTokenHandler)handler;
@end
