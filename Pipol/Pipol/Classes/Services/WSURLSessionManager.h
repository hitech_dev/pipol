//
//  WSURLSessionManager.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^WSURLSessionHandler)(id responseObject, NSURLResponse *response, NSError *error);
typedef void (^BlockSession)(id responseObject, NSURLResponse *response, NSError *error);

typedef void (^ServiceHandler)(BOOL success, NSString* error);
typedef void (^ServiceDataArrayHandler)(NSArray *result, NSString* error);
typedef void (^ServiceObjectHandler)(NSObject *object, NSString* error);
typedef void (^ServiceTokenHandler)(NSString *token, NSString* error);

@interface WSURLSessionManager : NSObject

+ (WSURLSessionManager*)shared;
- (void)sendRequest:(NSMutableURLRequest *)request clearCache:(BOOL)clearCache handler:(WSURLSessionHandler)handler;
- (void)updateFileWith:(NSDictionary*)params urlService:(NSString*)urlPath data:(NSData*)data name:(NSString*)name fileName:(NSString*)fileName mineType:(NSString*)mineType handler:(WSURLSessionHandler)handler;
@end
