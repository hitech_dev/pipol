//
//  WSURLSessionManager+City.m
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager+City.h"
#import "WSRequest.h"
#import "AppDefine.h"
#import "PPCityModel.h"
#import "PPProvinceModel.h"
#import "PPCountryModel.h"

#import <Mantle/Mantle.h>

@implementation WSURLSessionManager (City)

- (void)wsGetListCityWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler {
    [request setURL:[NSURL URLWithString:WS_CITY_LIST_CITY]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSArray *itemsArray = responseObject[@"Items"];
                    NSMutableArray *jobsArray = [[NSMutableArray alloc]init];
                    for (NSDictionary *obj in itemsArray) {
                        PPCityModel *cityModel = [MTLJSONAdapter modelOfClass:[PPCityModel class] fromJSONDictionary:obj error:nil];
                        if (cityModel) {
                            [jobsArray addObject:cityModel];
                        }
                    }
                    if (handler) {
                        handler(jobsArray, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            }  else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsGetListProvinceWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler {
    [request setURL:[NSURL URLWithString:WS_CITY_LIST_PROVINCE]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSArray *itemsArray = responseObject[@"Items"];
                    NSMutableArray *jobsArray = [[NSMutableArray alloc]init];
                    for (NSDictionary *obj in itemsArray) {
                        PPProvinceModel *provinceModel = [MTLJSONAdapter modelOfClass:[PPProvinceModel class] fromJSONDictionary:obj error:nil];
                        if (provinceModel) {
                            [jobsArray addObject:provinceModel];
                        }
                    }
                    if (handler) {
                        handler(jobsArray, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            }  else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsGetListCountryWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler {
    [request setURL:[NSURL URLWithString:WS_CITY_LIST_COUNTRY]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSArray *itemsArray = responseObject[@"Items"];
                    NSMutableArray *jobsArray = [[NSMutableArray alloc]init];
                    for (NSDictionary *obj in itemsArray) {
                        PPCountryModel *countryModel = [MTLJSONAdapter modelOfClass:[PPCountryModel class] fromJSONDictionary:obj error:nil];
                        if (countryModel) {
                            [jobsArray addObject:countryModel];
                        }
                    }
                    if (handler) {
                        handler(jobsArray, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            }  else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

@end
