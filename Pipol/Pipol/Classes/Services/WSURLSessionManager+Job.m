//
//  WSURLSessionManager+Job.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager+Job.h"
#import "WSRequest.h"
#import "AppDefine.h"
#import "PPJobModel.h"
#import "PPImageModel.h"
#import "PPAudioModel.h"

@implementation WSURLSessionManager (Job)
- (void)wsGetJobsPositionWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler {
    [request setURL:[NSURL URLWithString:WS_JOB_GET_JOBS_POSITION]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSArray *itemsArray = responseObject[@"Items"];
                    NSMutableArray *jobsArray = [[NSMutableArray alloc]init];
                    for (NSDictionary *obj in itemsArray) {
                        PPJobModel *jobModel = [MTLJSONAdapter modelOfClass:[PPJobModel class] fromJSONDictionary:obj error:nil];
                        if (jobModel) {
                            [jobsArray addObject:jobModel];
                        }
                    }
                    if (handler) {
                        handler(jobsArray, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            }  else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}
- (void)wsGetMyJobsWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler {
    [request setURL:[NSURL URLWithString:WS_JOB_GET_MY_JOBS]];
    [request setHTTPMethod:@"POST"];
    [self sendRequest:request clearCache:YES handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSArray *itemsArray = responseObject[@"Items"];
                    NSMutableArray *jobsArray = [[NSMutableArray alloc]init];
                    for (NSDictionary *jobObject in itemsArray) {
                        NSMutableDictionary *resultDiction = [[NSMutableDictionary alloc] initWithDictionary:jobObject];
                        NSDictionary *userDiction = resultDiction[@"User"];
                        NSMutableDictionary *editUserDiction = [[NSMutableDictionary alloc] initWithDictionary:userDiction];
                        if (editUserDiction[@"BirthDate"] == [NSNull null]) {
                            [editUserDiction setObject:@"" forKey:@"BirthDate"];
                        }
                        if (editUserDiction[@"FacebookId"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithLong:0] forKey:@"FacebookId"];
                        }
                        if (editUserDiction[@"DNI"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"DNI"];
                        }
                        if (editUserDiction[@"Gender"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"Gender"];
                        }
                        [resultDiction removeObjectForKey:@"User"];
                        [resultDiction setObject:editUserDiction forKey:@"User"];
                        PPJobModel *jobModel = [MTLJSONAdapter modelOfClass:[PPJobModel class] fromJSONDictionary:resultDiction error:nil];
                        if (jobModel) {
                            [jobsArray addObject:jobModel];
                        }
                    }
                    if (handler) {
                        handler(jobsArray, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}
- (void)wsGetJobIDWithRequest:(WSRequest *)request complition:(ServiceObjectHandler)handler {
    [request setURL:[NSURL URLWithString:WS_JOB_GET_JOBS_ID]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSDictionary *job = responseObject[@"Job"];
                    if (job) {
                        NSMutableDictionary *resultDiction = [[NSMutableDictionary alloc] initWithDictionary:job];
                        if (resultDiction[@"ApplyDate"] == [NSNull null]) {
                            [resultDiction setObject:[NSDate new] forKey:@"ApplyDate"];
                        }
                        PPJobModel *jobModel = [MTLJSONAdapter modelOfClass:[PPJobModel class] fromJSONDictionary:resultDiction error:nil];
                        if (handler) {
                            handler(jobModel, nil);
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No Job");
                        }
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}
- (void)wsApplyJobWithRequest:(WSRequest *)request complition:(ServiceHandler)handler {
    [request setURL:[NSURL URLWithString:WS_JOB_APPLY_JOB]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(false, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    if (handler) {
                        handler(true, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(false, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(false, @"Error");
            }
        }
    }];
}

#pragma mark: - Upload Image
- (void)wsUploadImageWith:(NSDictionary*)param serviceUrlPath:(NSString *)servicePath data:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName mineType:(NSString *)mineType complition:(ServiceObjectHandler)handler {
    [self updateFileWith:param urlService:servicePath data:data name:name fileName:fileName mineType:@"image/png" handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSDictionary *image = responseObject[@"Image"];
                    if (image) {
                        PPImageModel *imageModel = [MTLJSONAdapter modelOfClass:[PPImageModel class] fromJSONDictionary:image error:nil];
                        if (handler) {
                            if (imageModel) {
                                handler(imageModel, nil);
                            }
                            handler(nil, @"Can't not convert to model");
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No Image");
                        }
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

#pragma mark: - Upload Audio
- (void)wsUploadAudioWith:(NSDictionary*)param serviceUrlPath:(NSString *)servicePath data:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName mineType:(NSString *)mineType complition:(ServiceObjectHandler)handler {
    [self updateFileWith:param urlService:servicePath data:data name:name fileName:fileName mineType:mineType handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSDictionary *audio = responseObject[@"Audio"];
                    if (audio) {
                        PPAudioModel *audioModel = [MTLJSONAdapter modelOfClass:[PPAudioModel class] fromJSONDictionary:audio error:nil];
                        if (handler) {
                            if (audioModel) {
                                handler(audioModel, nil);
                            }
                            handler(nil, @"Can't not convert to model");
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No Audio");
                        }
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsAnswerQuizWithRequest:(WSRequest*)request  complition:(ServiceHandler)handler {
    [request setURL:[NSURL URLWithString:WS_JOB_ANSWER_QUIZ]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(false, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    if (handler) {
                        handler(true, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(false, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(false, @"Error");
            }
        }
    }];
}

- (void)wsFinishJobWithRequest:(WSRequest*)request  complition:(ServiceHandler)handler {
    [request setURL:[NSURL URLWithString:WS_JOB_FINISH_JOB]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(false, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    if (handler) {
                        handler(true, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(false, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(false, @"Error");
            }
        }
    }];
}

@end
