//
//  WSURLSessionManager+Login.m
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager+Login.h"
#import "WSRequest.h"
#import "AppDefine.h"
#import "PPUserModel.h"
#import <Mantle/Mantle.h>

@implementation WSURLSessionManager (Login)

- (void)wsLoginWithRequest:(WSRequest *)request handler:(ServiceTokenHandler)handler {
    [request setURL:[NSURL URLWithString:WS_LOGIN_LOGIN]];
    [self sendRequest:request clearCache:YES handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSString *token = responseObject[@"Token"];
                    [kUserDefaults setObject:token forKey:KEY_ACCESS_TOKEN];
                    [kUserDefaults synchronize];
                    
                    NSDictionary *user = responseObject[@"User"];
                    
                    if (user) {
                        NSData *userData = [NSJSONSerialization dataWithJSONObject:user options:0 error:nil];
                        if ([kUserDefaults objectForKey:KEY_PROFILE_EXIST]) {
                            [kUserDefaults removeObjectForKey:KEY_PROFILE_EXIST];
                        }
                        [kUserDefaults setObject:userData forKey:KEY_PROFILE_EXIST];
                        [kUserDefaults synchronize];
                        if (handler) {
                            handler(token, nil);
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No User");
                        }
                    }
                } else {
                    NSString *error = responseObject[@"Errors"][0];
                    if (handler) {
                        handler(nil, error);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsRegisterWithRequest:(WSRequest *)request handler:(ServiceTokenHandler)handler {
    [request setURL:[NSURL URLWithString:WS_LOGIN_REGISTER]];
    [self sendRequest:request clearCache:YES handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSString *token = responseObject[@"Token"];
                    [kUserDefaults setObject:token forKey:KEY_ACCESS_TOKEN];
                    [kUserDefaults synchronize];

                    NSDictionary *user = responseObject[@"User"];
                    
                    if (user) {
                        NSData *userData = [NSJSONSerialization dataWithJSONObject:user options:0 error:nil];
                        if ([kUserDefaults objectForKey:KEY_PROFILE_EXIST]) {
                            [kUserDefaults removeObjectForKey:KEY_PROFILE_EXIST];
                        }
                        [kUserDefaults setObject:userData forKey:KEY_PROFILE_EXIST];
                        [kUserDefaults synchronize];
                        if (handler) {
                            handler(token, nil);
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No User");
                        }
                    }
                } else {
                    NSString *error = responseObject[@"Errors"][0];
                    if (error) {
                        if (handler) {
                            handler(nil, error);
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"Error");
                        }
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
        
    }];
}

@end
