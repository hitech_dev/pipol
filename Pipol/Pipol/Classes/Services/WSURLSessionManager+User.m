//
//  WSURLSessionManager+User.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager+User.h"
#import "WSRequest.h"
#import "AppDefine.h"
#import "PPUserModel.h"
#import "PPBalanceModel.h"
#import <Mantle/Mantle.h>

@implementation WSURLSessionManager (User)

- (void)wsGetProfileWithRequest:(WSRequest *)request handler:(ServiceObjectHandler)handler {
    [self sendRequest:request clearCache:YES handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSDictionary *user = responseObject[@"User"];
                    if (user) {
                        NSMutableDictionary *editUserDiction = [[NSMutableDictionary alloc] initWithDictionary:user];
                        if (editUserDiction[@"BirthDate"] == [NSNull null]) {
                            [editUserDiction setObject:@"" forKey:@"BirthDate"];
                        }
                        if (editUserDiction[@"FacebookId"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithLong:0] forKey:@"FacebookId"];
                        }
                        if (editUserDiction[@"DNI"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"DNI"];
                        }
                        if (editUserDiction[@"Gender"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"Gender"];
                        }
                        if (editUserDiction) {
                            NSData *userData = [NSJSONSerialization dataWithJSONObject:editUserDiction options:0 error:nil];
                            if ([kUserDefaults objectForKey:KEY_PROFILE_EXIST]) {
                                [kUserDefaults removeObjectForKey:KEY_PROFILE_EXIST];
                            }
                            [kUserDefaults setObject:userData forKey:KEY_PROFILE_EXIST];
                            [kUserDefaults synchronize];
                        }
                        
                        PPUserModel *jobModel = [MTLJSONAdapter modelOfClass:[PPUserModel class] fromJSONDictionary:editUserDiction error:nil];
                        if (handler) {
                            handler(jobModel, nil);
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No User");
                        }
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsUploadProfileWithRequest:(WSRequest *)request handler:(ServiceObjectHandler)handler {
    [self sendRequest:request clearCache:YES handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSDictionary *user = responseObject[@"User"];
                    if (user) {
                        NSMutableDictionary *editUserDiction = [[NSMutableDictionary alloc] initWithDictionary:user];
                        if (editUserDiction[@"BirthDate"] == [NSNull null]) {
                            [editUserDiction setObject:@"" forKey:@"BirthDate"];
                        }
                        if (editUserDiction[@"FacebookId"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithLong:0] forKey:@"FacebookId"];
                        }
                        if (editUserDiction[@"DNI"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"DNI"];
                        }
                        if (editUserDiction[@"Gender"] == [NSNull null]) {
                            [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"Gender"];
                        }
                        if (editUserDiction) {
                            NSData *userData = [NSJSONSerialization dataWithJSONObject:editUserDiction options:0 error:nil];
                            if ([kUserDefaults objectForKey:KEY_PROFILE_EXIST]) {
                                [kUserDefaults removeObjectForKey:KEY_PROFILE_EXIST];
                            }
                            [kUserDefaults setObject:userData forKey:KEY_PROFILE_EXIST];
                            [kUserDefaults synchronize];
                        }
                        
                        PPUserModel *jobModel = [MTLJSONAdapter modelOfClass:[PPUserModel class] fromJSONDictionary:editUserDiction error:nil];
                        if (handler) {
                            handler(jobModel, nil);
                        }
                    } else {
                        if (handler) {
                            handler(nil, @"No User");
                        }
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsGetMyBalanceWithRequest:(WSRequest*)request hadler:(ServiceDataArrayHandler)handler {
    [request setURL:[NSURL URLWithString:WS_USER_BALANCE]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(nil, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    NSArray *itemsArray = responseObject[@"Items"];
                    NSMutableArray *jobsArray = [[NSMutableArray alloc]init];
                    for (NSDictionary *obj in itemsArray) {
                        NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithDictionary:obj];
                        NSDictionary *job = result[@"Job"];
                        NSMutableDictionary *resultJob = [[NSMutableDictionary alloc] initWithDictionary:job];
                        if (job) {
                            NSDictionary *user = job[@"User"];
                            NSMutableDictionary *editUserDiction = [[NSMutableDictionary alloc] initWithDictionary:user];
                            if (editUserDiction[@"BirthDate"] == [NSNull null]) {
                                [editUserDiction setObject:@"" forKey:@"BirthDate"];
                            }
                            if (editUserDiction[@"FacebookId"] == [NSNull null]) {
                                [editUserDiction setObject:[NSNumber numberWithLong:0] forKey:@"FacebookId"];
                            }
                            if (editUserDiction[@"DNI"] == [NSNull null]) {
                                [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"DNI"];
                            }
                            if (editUserDiction[@"Gender"] == [NSNull null]) {
                                [editUserDiction setObject:[NSNumber numberWithInt:0] forKey:@"Gender"];
                            }
                            [resultJob removeObjectForKey:@"User"];
                            [resultJob setObject:editUserDiction forKey:@"User"];
                        }
                        [result removeObjectForKey:@"Job"];
                        [result setObject:resultJob forKey:@"Job"];
                        PPBalanceModel *balanceModel = [MTLJSONAdapter modelOfClass:[PPBalanceModel class] fromJSONDictionary:result error:nil];
                        if (balanceModel) {
                            [jobsArray addObject:balanceModel];
                        }
                    }
                    if (handler) {
                        handler(jobsArray, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(nil, errorsArray[0]);
                    }
                }
            }  else if (handler) {
                handler(nil, @"Error");
            }
        }
    }];
}

- (void)wsRecallMoneyWithRequest:(WSRequest*)request hadler:(ServiceHandler)handler {
    [request setURL:[NSURL URLWithString:WS_USER_RECALL_MONEY]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(false, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    if (handler) {
                        handler(true, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(false, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(false, @"Error");
            }
        }
    }];
}

- (void)wsUploadProfileImageWithRequest:(WSRequest*)request hadler:(ServiceHandler)handler {
    [request setURL:[NSURL URLWithString:WS_USER_RECALL_MONEY]];
    [self sendRequest:request clearCache:NO handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (error) {
            if (handler) {
                handler(false, @"Can not connect to server!");
            }
        } else {
            if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
                BOOL result = [responseObject[@"Result"] boolValue];
                if (result) {
                    if (handler) {
                        handler(true, nil);
                    }
                } else {
                    NSArray *errorsArray = responseObject[@"Errors"];
                    if (errorsArray && errorsArray.count > 0) {
                        handler(false, errorsArray[0]);
                    }
                }
            } else if (handler) {
                handler(false, @"Error");
            }
        }
    }];
}

@end
