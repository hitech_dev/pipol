//
//  WSURLSessionManager+User.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager.h"
@class WSRequest;

@interface WSURLSessionManager (User)

- (void)wsGetProfileWithRequest:(WSRequest *)request handler:(ServiceObjectHandler)handler;
- (void)wsUploadProfileWithRequest:(WSRequest *)request handler:(ServiceObjectHandler)handler;
- (void)wsGetMyBalanceWithRequest:(WSRequest*)request hadler:(ServiceDataArrayHandler)handler;
- (void)wsRecallMoneyWithRequest:(WSRequest*)request hadler:(ServiceHandler)handler;
- (void)wsUploadProfileImageWithRequest:(WSRequest*)request hadler:(ServiceHandler)handler;
@end
