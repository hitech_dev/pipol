//
//  WSURLSessionManager+Job.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager.h"
@class WSRequest;

@interface WSURLSessionManager (Job)

- (void)wsGetJobsPositionWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler;
- (void)wsGetMyJobsWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler;
- (void)wsGetJobIDWithRequest:(WSRequest *)request complition:(ServiceObjectHandler)handler;
- (void)wsApplyJobWithRequest:(WSRequest *)request complition:(ServiceHandler)handler;

- (void)wsUploadImageWith:(NSDictionary*)param serviceUrlPath:(NSString *)servicePath data:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName mineType:(NSString *)mineType complition:(ServiceObjectHandler)handler;
- (void)wsUploadAudioWith:(NSDictionary*)param serviceUrlPath:(NSString *)servicePath data:(NSData *)data name:(NSString *)name fileName:(NSString *)fileName mineType:(NSString *)mineType complition:(ServiceObjectHandler)handler;

- (void)wsAnswerQuizWithRequest:(WSRequest*)request  complition:(ServiceHandler)handler;
- (void)wsFinishJobWithRequest:(WSRequest*)request  complition:(ServiceHandler)handler;
@end
