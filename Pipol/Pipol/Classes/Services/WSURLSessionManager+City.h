//
//  WSURLSessionManager+City.h
//  Pipol
//
//  Created by HiTechLtd on 3/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager.h"
@class WSRequest;

@interface WSURLSessionManager (City)

- (void)wsGetListCityWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler;
- (void)wsGetListProvinceWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler;
- (void)wsGetListCountryWithRequest:(WSRequest *)request complition:(ServiceDataArrayHandler)handler;
@end
