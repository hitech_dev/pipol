//
//  WSURLSessionManager.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSURLSessionManager.h"
#import "AFNetworking.h"
#import "NSString+Help.h"
#import "AppDefine.h"
#import "NSDictionary+NotNull.h"
#import "Pipol-PrefixHeader.pch"

@interface WSURLSessionManager ()
{
    BOOL _isCheckUnknownError;
}
@property (nonatomic, strong) NSOperationQueue *serviceQueue;
@property (nonatomic, strong) NSURLSession *urlSession;

@end

@implementation WSURLSessionManager

+ (WSURLSessionManager*)shared {
    static WSURLSessionManager *instance = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        instance = [[self alloc]init];
    });
    return instance;
}

- (void)sendRequest:(NSMutableURLRequest *)request clearCache:(BOOL)clearCache handler:(WSURLSessionHandler)handler {
    DBG(@"WS-REQUEST-URL: %@",request.URL.absoluteString);
    DBG(@"WS-REQUEST-METHOD: %@",request.HTTPMethod);
    DBG(@"WS-REQUEST-BODY: %@",[[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding]);
    [self callSessionRequest:request clearCache:clearCache handler:^(id responseObject, NSURLResponse *response, NSError *error) {
        if (handler) {
            handler (responseObject,response,error);
        }
    }];
}

- (void)callSessionRequest:(NSMutableURLRequest *)request clearCache:(BOOL)clearCache handler:(BlockSession)handler {
    NSURLSessionConfiguration *urlSessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *sm = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:urlSessionConfig];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    securityPolicy.validatesDomainName = NO;
    sm.securityPolicy = securityPolicy;
    [sm setResponseSerializer:[AFJSONResponseSerializer serializer]];
    if (clearCache) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    }
    
    [[sm dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //check if error is exist
        NSInteger statusCodeWS = httpResponse.statusCode;
        DBG(@"Status_CodeWS %tu",statusCodeWS);
        if (statusCodeWS >= 300) {
            NSString *errorMessage = error.localizedDescription;
            if (responseObject) {
                NSString *strError = [responseObject objectForKeyNotNull:@"detail"];
                if (strError && [strError isEqualToString:ERROR_AUTH_NOT_PROVIDED]) {
                    errorMessage = ERROR_AUTH_NOT_PROVIDED;
                }
                strError = [responseObject objectForKeyNotNull:@"message"];
                if (strError) {
                    errorMessage = strError;
                }
                strError = [responseObject objectForKeyNotNull:@"error"];
                if (strError) {
                    errorMessage = strError;
                }
            }
            
            NSInteger errorCode = statusCodeWS;
            NSError *error = [NSError errorWithDomain:WS_ERROR_DOMAIN
                                                 code:errorCode
                                             userInfo:@{@"message":errorMessage}];
            if(handler) {
                handler(responseObject,response,error);
            }
            return ;
        }
        if (responseObject && !([responseObject isKindOfClass:[NSDictionary class]] || [responseObject isKindOfClass:[NSArray class]])) {
            NSInteger errorCode = 500;
            NSString *errorMessage = @"The Response is invalid.";
            NSError *error = [NSError errorWithDomain:WS_ERROR_DOMAIN
                                                 code:errorCode
                                             userInfo:@{@"message":errorMessage}];
            if(handler) {
                handler(responseObject,response,error);
            }
        } else {
            if (error) {
                NSString *errorCode = [NSString stringWithFormat:@"%tu",error.code];
                NSString *errorMessage = error.localizedDescription;
                NSError *error = [NSError errorWithDomain:WS_ERROR_DOMAIN
                                                     code:0
                                                 userInfo:@{@"message":errorMessage, @"code":errorCode}];
                if(handler) {
                    handler(responseObject,response,error);
                }
            } else {
                if(handler) {
                    handler(responseObject,response,nil);
                }
            }
        }
    }] resume];
}

- (void)updateFileWith:(NSDictionary*)params urlService:(NSString*)urlPath data:(NSData*)data name:(NSString*)name fileName:(NSString*)fileName mineType:(NSString*)mineType handler:(WSURLSessionHandler)handler {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setTaskDidSendBodyDataBlock:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        //during the progress
    }];
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]  multipartFormRequestWithMethod:@"POST" URLString:urlPath parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"File" fileName:fileName mimeType:mineType];
    } error:nil];
    NSURLSessionDataTask *uploadTask = [manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        //check if error is exist
        NSInteger statusCodeWS = httpResponse.statusCode;
        DBG(@"Status_CodeWS %tu",statusCodeWS);
        if (statusCodeWS >= 300) {
            NSString *errorMessage = error.localizedDescription;
            if (responseObject) {
                NSString *strError = [responseObject objectForKeyNotNull:@"detail"];
                if (strError && [strError isEqualToString:ERROR_AUTH_NOT_PROVIDED]) {
                    errorMessage = ERROR_AUTH_NOT_PROVIDED;
                }
                strError = [responseObject objectForKeyNotNull:@"message"];
                if (strError) {
                    errorMessage = strError;
                }
                strError = [responseObject objectForKeyNotNull:@"error"];
                if (strError) {
                    errorMessage = strError;
                }
            }
            
            NSInteger errorCode = statusCodeWS;
            NSError *error = [NSError errorWithDomain:WS_ERROR_DOMAIN
                                                 code:errorCode
                                             userInfo:@{@"message":errorMessage}];
            if(handler) {
                handler(responseObject,response,error);
            }
            return ;
        }
        if (responseObject && !([responseObject isKindOfClass:[NSDictionary class]] || [responseObject isKindOfClass:[NSArray class]])) {
            NSInteger errorCode = 500;
            NSString *errorMessage = @"The Response is invalid.";
            NSError *error = [NSError errorWithDomain:WS_ERROR_DOMAIN
                                                 code:errorCode
                                             userInfo:@{@"message":errorMessage}];
            if(handler) {
                handler(responseObject,response,error);
            }
        } else {
            if (error) {
                NSString *errorCode = [NSString stringWithFormat:@"%tu",error.code];
                NSString *errorMessage = error.localizedDescription;
                NSError *error = [NSError errorWithDomain:WS_ERROR_DOMAIN
                                                     code:0
                                                 userInfo:@{@"message":errorMessage, @"code":errorCode}];
                if(handler) {
                    handler(responseObject,response,error);
                }
            } else {
                if(handler) {
                    handler(responseObject,response,nil);
                }
            }
        }
    }];
    [uploadTask resume];
    
}
@end