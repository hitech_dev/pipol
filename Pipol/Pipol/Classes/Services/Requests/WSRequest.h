//
//  WSRequest.h
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSRequest : NSMutableURLRequest

- (void)setBodyParam:(NSDictionary*)body;
- (NSString*)stringParamWithParam:(NSString*)param key:(NSString*)key;

@end
