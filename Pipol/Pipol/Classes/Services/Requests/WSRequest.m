//
//  WSRequest.m
//  Pipol
//
//  Created by HiTechLtd on 3/7/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "WSRequest.h"
#import "NSString+Help.h"
#import "AppDefine.h"

@implementation WSRequest

- (id)init {
    self = [super init];
    if (self) {
        [self setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        [self setTimeoutInterval:WS_TIME_OUT];
        [self setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self setHTTPMethod:@"POST"];
    }
    return self;
}

#pragma mark - func set
- (void)setBodyParam:(NSDictionary*)body {
    NSData *data = [NSJSONSerialization dataWithJSONObject:body options:0 error:nil];
    [self setHTTPBody:data];
}

- (NSString*)stringParamWithParam:(NSString*)param key:(NSString*)key {
    return [NSString stringWithFormat:@"%@=%@",key,[param urlEncode]];
}
@end