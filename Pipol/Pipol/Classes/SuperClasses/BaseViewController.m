//
//  BaseViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/24/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "BaseViewController.h"
#import "SWRevealViewController.h"
#import "ScaleLayoutConstraint.h"
#import "AppDefine.h"
#import "NSObject+NSObjectExtension.h"

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationController *navigationController = self.navigationController;
    if (navigationController != nil) {
        [self defaultTypeNavigation];
    }
    [self updateAllConstraintsWithRatio: ScreenScale];
}
- (void)defaultTypeNavigation {
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSMutableDictionary *diction = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = diction;
    self.navigationController.navigationBar.barTintColor = [UIColor purpleColor];
}

- (void)setupWhiteBarOfNavigation {
    NSMutableDictionary *diction = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor purpleColor], NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = diction;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
}

- (void)defaultRevealViewControllerOnNavigation {
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu.png"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector( revealToggle: )];
        [self.revealViewController revealToggleAnimated:YES];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu.png"] style:UIBarButtonItemStylePlain target:self action:@selector(gotoMenu: )];
    }
}

- (void)gotoMenu:(UIButton*)sender {
    UIStoryboard *mainSto = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *vc = [mainSto instantiateViewControllerWithIdentifier:[SWRevealViewController identifier]];
    kWindow.rootViewController = vc;
    [kWindow makeKeyAndVisible];
}

- (void)setupBarButtonSave {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"GUARDAR" style:UIBarButtonItemStylePlain target:self action:@selector(actionSave:)];
}

- (void)actionSave:(UIBarButtonItem*)sender {
    // save
}

-(void)customBackButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"ic_nav_back.png"];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, 12, 10);
    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(__popVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:aBarButtonItem];
}

- (void)customBlackBackButton {
    UIImage *buttonImage = [UIImage imageNamed:@"ic_nav_back_black.png"];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, 12, 10);
    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(__popVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:aBarButtonItem];
}

#pragma mark: - scale
- (void)updateAllConstraintsWithRatio:(CGFloat)ratio {
    for (NSLayoutConstraint *constraint in self.view.constraints) {
        if ([constraint isKindOfClass: [ScaleLayoutConstraint class]]) {
            constraint.constant = constraint.constant * ratio;
        }
    }
    [self updateAllConstraintsWithRatio:ratio forView:self.view];
}

- (void)updateAllConstraintsWithRatio:(CGFloat)ratio forView:(UIView*)view {
    for (UIView *subView in view.subviews) {
        for (NSLayoutConstraint *constraint in subView.constraints) {
            if ([constraint isKindOfClass: [ScaleLayoutConstraint class]]) {
                constraint.constant = constraint.constant * ratio;
            }
        }
        [self updateAllConstraintsWithRatio:ratio forView:subView];
    }
}

#pragma mark: - actions
- (void)__popVC:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)popViewControllerWithTime:(CGFloat)time {
    [UIView animateWithDuration:time animations:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
@end

