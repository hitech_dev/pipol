//
//  BaseViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/24/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)defaultTypeNavigation;
- (void)setupWhiteBarOfNavigation;
- (void)defaultRevealViewControllerOnNavigation;
- (void)setupBarButtonSave;
- (void)actionSave:(UIButton*)sender;
- (void)customBackButton;
- (void)customBlackBackButton;
- (void)popViewControllerWithTime:(CGFloat)time;
- (void)gotoMenu:(UIButton*)sender;
@end
