//
//  BaseNavigationController.m
//  Pipol
//
//  Created by HiTechLtd on 2/24/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "BaseNavigationController.h"

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.translucent = NO;
    self.navigationBar.shadowImage = nil;
}

@end
