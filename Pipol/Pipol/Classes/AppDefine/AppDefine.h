//
//  AppDefine.h
//  Pipol
//
//  Created by HiTechLtd on 3/6/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#ifndef Pipol_AppDefine_h
#define Pipol_AppDefine_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define URL_API                         @"http://pipol.estefanosalazar.com/api/"
#define URL_IMAGE                       @"http://pipol.estefanosalazar.com/Archivos/ImagenesPerfiles/"
#define URL_AUDIO                       @"http://pipol.estefanosalazar.com/Archivos/Audios/"
#define URL_IMAGE_JOB                   @"http://pipol.estefanosalazar.com/Archivos/Imagenes/"
#define WS_TIME_OUT                     120


#define ERROR_AUTH_NOT_PROVIDED         @"Authentication credentials were not provided."
#define WS_ERROR_DOMAIN                 @"PIPOL_ERROR_DOMAIN"

#define CLIENT_ID                       @"AoB2Dn2P93FFYkd2Hcd15opIaC9lIn8ciIPNg44O"
#define CLIENT_SECRECT                              @"E5SVOzDAICZ2fUJBx8uWFfb7eUZumkZ9QrSoCsLRgvAAQVEdMQ98TWyZdF07rQLbpX0sbJETOxsXJgoy2pUbpYlEQFnvHguPkFEH92fwHiAR2p6Yhxf1hwdTGkCruBKF"

#define KEY_USER_AVATAR                 @"UserAvatar"
#define KEY_USER_COVER_IMAGE            @"UserCoverImage"
#define KEY_USER_ID                     @"UserId"
#define KEY_ACCESS_TOKEN                @"FacebookToken"
#define KEY_FACEBOOK_ID                 @"FacebookId"
#define KEY_PROFILE_EXIST               @"PROFILE_EXIST"
#define KEY_PASSWORD_EXIST              @"KEY_PASSWORD_EXIST"

#define IS_SHOW_TUTORIAL_PIPOL          @"PIPOL_IS_SHOW_TUTORIAL"
#define IS_ACCEPT_TERMS_PIPOL           @"IS_ACCEPT_TERMS_PIPOL"

//Login
#define WS_LOGIN_LOGIN                  [URL_API stringByAppendingString:@"Login/"]
#define WS_LOGIN_REGISTER               [URL_API stringByAppendingString:@"Login/Register/"]

//Job
#define WS_JOB_GET_JOBS_POSITION        [URL_API stringByAppendingString:@"Jobs/GetByPosition"]
#define WS_JOB_GET_MY_JOBS              [URL_API stringByAppendingString:@"Jobs/"]
#define WS_JOB_GET_JOBS_ID              [URL_API stringByAppendingString:@"Jobs/GetById/"]
#define WS_JOB_APPLY_JOB                [URL_API stringByAppendingString:@"Jobs/ApplyJob/"]

#define WS_JOB_UP_IMAGE                 [URL_API stringByAppendingString:@"Jobs/UploadImage/"]
#define WS_JOB_UP_AUDIO                 [URL_API stringByAppendingString:@"Jobs/UploadAudio/"]

#define WS_JOB_ANSWER_QUIZ              [URL_API stringByAppendingString:@"Jobs/AnswerQuiz/"]
#define WS_JOB_FINISH_JOB               [URL_API stringByAppendingString:@"Jobs/Finish/"]

//User
#define WS_USER_GET_PROFILE             [URL_API stringByAppendingString:@"Users/Me/"]
#define WS_USER_UPDATE_PROFILE          [URL_API stringByAppendingString:@"/Users/UpdateProfile/"]
#define WS_USER_BALANCE                 [URL_API stringByAppendingString:@"/Users/MyBalance/"]
#define WS_USER_RECALL_MONEY            [URL_API stringByAppendingString:@"/Users/Recall/"]
#define WS_USER_UPLOAD_IMAGE            [URL_API stringByAppendingString:@"/Users/UploadImage/"]

// City
#define WS_CITY_LIST_CITY               [URL_API stringByAppendingString:@"/Cities/"]
#define WS_CITY_LIST_PROVINCE           [URL_API stringByAppendingString:@"/Provinces/"]
#define WS_CITY_LIST_COUNTRY            [URL_API stringByAppendingString:@"/Countries/"]

#define ScreenScale                     [[UIScreen mainScreen] bounds].size.width/414.0
//ENUM
typedef NS_ENUM(NSInteger, LoginType) {
    LoginTypeNone,
    LoginTypeFacebook,
    LoginTypeEmail
};

#define m_string(str)                           (NSLocalizedString(str, nil))

//DEFAULT KEY
#define kUserDefaults                   [NSUserDefaults standardUserDefaults]
#define kMainQueue                      [NSOperationQueue mainQueue]
#define kWindow                         [[UIApplication sharedApplication] keyWindow]

#endif /* AppDefine_h */
