//
//  StartJobCell.m
//  Pipol
//
//  Created by HiTechLtd on 3/1/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "StartJobCell.h"
#import "PPQuizModel.h"
#import "NSString+Help.h"

@interface StartJobCell() {
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIImageView *imgStick;
}

@end

@implementation StartJobCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setQuizModel:(PPQuizModel *)quizModel {
    _quizModel = quizModel;
    if (_quizModel) {
        if (_quizModel.quizContent && ![_quizModel.quizContent isEmpty]) {
            lblTitle.text = [[NSString stringWithFormat:@"%d. ", self.indexPath.row + 1] stringByAppendingString: _quizModel.quizContent.uppercaseString];
        } else {
            lblTitle.text = @"";
        }
        if (_quizModel.quizAnswer && ![ _quizModel.quizAnswer isEmpty]) {
            imgStick.image = [UIImage imageNamed:@"ic_preguntas_answered.png"];
        }
    }
}
@end
