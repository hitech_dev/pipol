//
//  JobCell.h
//  Pipol
//
//  Created by HiTechLtd on 2/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CircleImageView;
@class PPJobModel;

@interface JobCell : UITableViewCell

@property (nonatomic, strong) PPJobModel *jobModel;

- (void)showPendingView;
- (void)showCompleteView;
@end
