//
//  MoneyCell.m
//  Pipol
//
//  Created by HiTechLtd on 2/27/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MoneyCell.h"
#import "PPBalanceModel.h"
#import "PPJobModel.h"
#import "PPJobBranchModel.h"
#import "UIImageView+AFNetworking.h"
#import "AppDefine.h"
#import "CircleImageView.h"

@implementation MoneyCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setBalanceModel:(PPBalanceModel *)balanceModel {
    _balanceModel = balanceModel;
    self.lbljobName.text = _balanceModel.balanceJob.jobTitle;
    if (_balanceModel.balanceJob.jobFinished) {
        self.lblJobState.text = @"Completado";
    } else {
        self.lblJobState.text = @"Pendiente";
    }
    self.lblPrice.text = [NSString stringWithFormat:@"%d", _balanceModel.balanceDecimal];
    [self.imgvAvatar setImageWithURL:[NSURL URLWithString:[URL_IMAGE stringByAppendingString: _balanceModel.balanceJob.jobImage]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"]];
}

@end
