//
//  JobCell.m
//  Pipol
//
//  Created by HiTechLtd on 2/26/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "JobCell.h"
#import "PPJobModel.h"
#import "AppDefine.h"
#import "CircleImageView.h"
#import "PPImageModel.h"
#import "PPJobModel.h"
#import "PPJobBranchModel.h"
#import "UIImageView+AFNetworking.h"
#import "UIHelpers.h"

@interface JobCell() {
    __weak IBOutlet CircleImageView *imgvAvatar;
    __weak IBOutlet UILabel *lblJobName;
    __weak IBOutlet UIView *pendingView;
    __weak IBOutlet UIView *completeView;
    __weak IBOutlet UILabel *lblPendingTime;
    __weak IBOutlet UILabel *lblPrice;
    NSTimer *timer;
    NSDate *applyDate;
}

@end

@implementation JobCell

- (void)awakeFromNib {
    [self refreshUI];
}

- (void)setJobModel:(PPJobModel *)jobModel {
    _jobModel = jobModel;
    [self refreshUI];
}

#pragma mark - show views
- (void)showPendingView {
    pendingView.hidden = NO;
    completeView.hidden = YES;
    applyDate = _jobModel.jobApplyDate;
    [kMainQueue addOperationWithBlock:^{
        [self refreshPedingTime:applyDate];
    }];
}

- (void)showCompleteView {
    pendingView.hidden = YES;
    completeView.hidden = NO;
    lblPrice.text = [NSString stringWithFormat:@"%tu", _jobModel.jobPayAmount];
}

#pragma mark: refreshs
- (void)refreshUI {
    [kMainQueue addOperationWithBlock:^{
        if (_jobModel) {
            if (_jobModel.jobImage) {
                [imgvAvatar setImageWithURL:[NSURL URLWithString:[URL_IMAGE stringByAppendingString: _jobModel.jobImage]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"]];
            }
            lblJobName.text = _jobModel.jobTitle;
            if (_jobModel.jobFinished) {
                [self showCompleteView];
            } else {
                [self showPendingView];
            }
        } else {
            imgvAvatar.image = [UIImage imageNamed:@""];
            lblJobName.text = @" ";
        }
    }];
}

- (void)refreshPedingTime:(NSDate*)completeDate {
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(refreshTime) userInfo:nil repeats:YES];
    lblPendingTime.text = [UIHelpers stringTimeFromDate:[NSDate date] intoDate:completeDate];
}


- (void)refreshTime {
    NSString *timeString = [UIHelpers stringTimeFromDate:[NSDate date] intoDate:applyDate];
    if (timeString == nil) {
        [timer invalidate];
        timer = nil;
        lblPendingTime.text = @"expirar";
    } else {
        lblPendingTime.text = timeString;
    }
}

@synthesize jobModel = _jobModel;
@end
