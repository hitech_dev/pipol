//
//  StartJobCell.h
//  Pipol
//
//  Created by HiTechLtd on 3/1/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PPQuizModel;

@interface StartJobCell : UITableViewCell

@property (nonatomic, strong) PPQuizModel *quizModel;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end
