//
//  ResponderAnswerCell.m
//  Pipol
//
//  Created by HiTechLtd on 3/2/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ResponderAnswerCell.h"

@implementation ResponderAnswerCell

- (void)awakeFromNib {
    
}

- (IBAction)__actionRemoveAnswer:(id)sender {
    if ([self.delegate respondsToSelector:@selector(removeAnswerAtCell:)]) {
        [self.delegate removeAnswerAtCell:self];
    }
}

@end
