//
//  ResponderQuestionCell.m
//  Pipol
//
//  Created by HiTechLtd on 3/2/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ResponderQuestionCell.h"

@implementation ResponderQuestionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setOrder:(NSInteger)order {
    _order = order;
    self.lblOrder.text = [NSString stringWithFormat:@"%d", _order + 1];
    NSLog(@"index: %@", self.lblOrder.text);
}

- (void)setQuizContent:(NSString *)quizContent {
    _quizContent = quizContent;
    self.tvQuestion.text = _quizContent;
}

@end
