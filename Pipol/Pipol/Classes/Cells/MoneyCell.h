//
//  MoneyCell.h
//  Pipol
//
//  Created by HiTechLtd on 2/27/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CircleImageView;
@class PPBalanceModel;

@interface MoneyCell : UITableViewCell

@property (nonatomic, weak) IBOutlet CircleImageView *imgvAvatar;
@property (nonatomic, weak) IBOutlet UILabel *lbljobName;
@property (nonatomic, weak) IBOutlet UILabel *lblJobState;
@property (nonatomic, weak) IBOutlet UILabel *lblPrice;

@property (nonatomic, strong) PPBalanceModel *balanceModel;
@end
