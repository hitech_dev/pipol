//
//  ResponderAnswerCell.h
//  Pipol
//
//  Created by HiTechLtd on 3/2/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ResponderAnswerCell;

@protocol ResponderAnswerCellDelegate <NSObject>
@optional
- (void)removeAnswerAtCell:(ResponderAnswerCell *)cell;
@end

@interface ResponderAnswerCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) IBOutlet UITextView *tvAnswer;
@property (nonatomic, weak) id<ResponderAnswerCellDelegate>delegate;
@end
