//
//  ResponderQuestionCell.h
//  Pipol
//
//  Created by HiTechLtd on 3/2/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResponderQuestionCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblOrder;
@property (nonatomic, weak) IBOutlet UITextView *tvQuestion;
@property (nonatomic, strong)  NSString *quizContent;
@property (nonatomic, assign)  NSInteger order;
@end
