//
//  PPJobManager.m
//  Pipol
//
//  Created by HiTechLtd on 3/10/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPJobManager.h"
#import "WSURLSessionManager.h"
#import "WSURLSessionManager+Job.h"
#import "WSRequest.h"
#import "AppDefine.h"
#import "PPJobModel.h"
#import <Mantle/Mantle.h>

@implementation PPJobManager

+(instancetype)shared {
    static PPJobManager *instance = nil;
    static dispatch_once_t oneTOken;
    dispatch_once(&oneTOken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)getJobsByPositionLatitude:(double)latitude longitude:(double)longitude distance:(double)distance complition:(JobDataArrayHandler)handler {
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (token) {
        WSRequest *request = [[WSRequest alloc] init];
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:token forKey:@"Token"];
        [body setObject:[NSNumber numberWithDouble:latitude] forKey:@"Latitude"];
        [body setObject:[NSNumber numberWithDouble:longitude] forKey:@"Longitude"];
        [body setObject:[NSNumber numberWithDouble:distance] forKey:@"Distance"];
        [request setBodyParam:body];
        [[WSURLSessionManager shared] wsGetJobsPositionWithRequest:request complition:^(NSArray *result, NSString *error) {
            if (error) {
                if (handler) {
                    handler(nil, error);
                }
            } else {
                if (handler) {
                    handler(result, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(nil, @"Error");
        }
    }
}
- (void)getMyJobsWithComplition:(JobDataArrayHandler)handler {
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    WSRequest *request = [[WSRequest alloc] init];
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (token) {
        [body setObject:token forKey:@"Token"];
    } else {
        [body setObject:[NSNull null] forKey:@"Token"];
    }
    [request setBodyParam:body];
    //call api
    [[WSURLSessionManager shared] wsGetMyJobsWithRequest:request complition:^(NSArray *result, NSString *error) {
        if (error) {
            if (handler) {
                handler(nil, error);
            }
        } else {
            if (handler) {
                handler(result, nil);
            }
        }
    }];
}
- (void)getJobWithID:(NSInteger)jobID complition:(JobObjectHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (token) {
        [body setObject:token forKey:@"Token"];
    } else {
        [body setObject:[NSNull null] forKey:@"Token"];
    }
    [body setObject:[NSNumber numberWithInteger:jobID] forKey:@"Id"];
    [request setBodyParam:body];
    //call api
    [[WSURLSessionManager shared] wsGetJobIDWithRequest:request complition:^(NSObject *object, NSString *error) {
        if (error) {
            if (handler) {
                handler(nil, error);
            }
        } else {
            if (handler) {
                handler(object, nil);
            }
        }
    }];
}
- (void)applyJobWithID:(NSInteger)jobID complition:(JobHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (jobID && token) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:token forKey:@"Token"];
        [body setObject:[NSNumber numberWithInteger:jobID] forKey:@"Id"];
        [request setBodyParam:body];
    }
    //call api
    [[WSURLSessionManager shared] wsApplyJobWithRequest:request complition:^(BOOL success, NSString *error) {
        if (error) {
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}

#pragma mark: - upload image
- (void)uploadImageWithID:(NSInteger)jobID image:(UIImage*)image imageName:(NSString*)imageName  complition:(JobObjectHandler)handler {
    
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (jobID && token && image) {
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:[NSNumber numberWithInteger:jobID] forKey:@"Id"];
        [param setObject:token forKey:@"Token"];
        NSString *url = WS_JOB_UP_IMAGE;
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        [[WSURLSessionManager shared] wsUploadImageWith:param serviceUrlPath:url data:imageData name:@"File" fileName:imageName mineType:@"image/png" complition:^(NSObject *object, NSString *error) {
            if (error) {
                if (handler) {
                    handler(nil, error);
                }
            } else {
                if (handler) {
                    handler(object, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(nil, @"Token, image or param are invalid!");
        }
    }
}

#pragma mark: - upload audio
- (void)uploadAudioWithJobID:(NSInteger)jobID audioPath:(NSString*)audioPath audioName:(NSString*)audioName  complition:(JobObjectHandler)handler; {
    
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (jobID && token && audioPath) {
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:[NSNumber numberWithInteger:jobID] forKey:@"Id"];
        [param setObject:token forKey:@"Token"];
        [param setObject:audioName forKey:@"File"];
        NSData *audioData = [[NSFileManager defaultManager] contentsAtPath:audioPath];
        NSString *url = WS_JOB_UP_AUDIO;
        
        NSLog(@"%@",param);
        
        if(param && audioData && audioName) {
            [[WSURLSessionManager shared] wsUploadAudioWith:param serviceUrlPath:url data:audioData name:@"File" fileName:audioName mineType:@"audio/mp3" complition:^(NSObject *object, NSString *error) {
                if (error) {
                    if (handler) {
                        handler(nil, error);
                    }
                } else {
                    if (handler) {
                        handler(object, nil);
                    }
                }
            }];
        } else {
            if (handler) {
                handler(nil, @"Can not upload data!");
            }
        }
    } else {
        if (handler) {
            handler(nil, @"Token, image or param are invalid!");
        }
    }
}

#pragma mark: - Answer Quiz
- (void)answerQuizWithQuizID:(NSInteger)quizID answer:(NSString*)answer complition:(JobHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (quizID && token) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:token forKey:@"Token"];
        [body setObject:[NSNumber numberWithInteger:quizID] forKey:@"QuizId"];
        if (answer) {
            [body setObject:answer forKey:@"Answer"];
        } else {
            [body setObject:[NSNull null] forKey:@"Answer"];
        }
        [request setBodyParam:body];
    }
    //call api
    [[WSURLSessionManager shared] wsAnswerQuizWithRequest:request complition:^(BOOL success, NSString *error) {
        if (error) {
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}

- (void)finishJobWithJobID:(NSInteger)jobID complition:(JobHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (jobID && token) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:token forKey:@"Token"];
        [body setObject:[NSNumber numberWithInteger:jobID] forKey:@"JobId"];
        [request setBodyParam:body];
    }
    //call api
    [[WSURLSessionManager shared] wsFinishJobWithRequest:request complition:^(BOOL success, NSString *error) {
        if (error) {
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}

@end
