//
//  LoginManager.m
//  Pipol
//
//  Created by HiTechLtd on 3/3/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "LoginManager.h"
#import "AppDefine.h"
#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "WSURLSessionManager+Login.h"
#import "WSRequest.h"
#import "PPUserModel.h"
#import <Mantle/Mantle.h>

@interface LoginManager() {
    LoginType _type;
}

@end

@implementation LoginManager

+(instancetype)shared {
    static LoginManager *instance = nil;
    static dispatch_once_t oneTOken;
    dispatch_once(&oneTOken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}
//||
- (BOOL)isLoggedIn {
    id token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    return token && [token isKindOfClass:[NSString class]];
}

- (LoginType)loginType {
    return _type;
}

- (PPUserModel*)modelUserProfile {
    NSData *userData = [kUserDefaults objectForKey:KEY_PROFILE_EXIST];
    if (userData) {
        NSDictionary *userDiction = [NSJSONSerialization JSONObjectWithData:userData options:0 error:nil];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:userDiction];
        if (dic[@"BirthDate"] == [NSNull null]) {
            [dic setObject:@"" forKey:@"BirthDate"];
        }
        if (dic[@"FacebookId"] == [NSNull null]) {
            [dic setObject:[NSNumber numberWithLong:0] forKey:@"FacebookId"];
        }
        if (dic[@"DNI"] == [NSNull null]) {
            [dic setObject:[NSNumber numberWithInt:0] forKey:@"DNI"];
        }
        if (dic[@"Gender"] == [NSNull null]) {
            [dic setObject:[NSNumber numberWithInt:0] forKey:@"Gender"];
        }
        PPUserModel *userModel = [MTLJSONAdapter modelOfClass:[PPUserModel class] fromJSONDictionary:dic error:nil];
        return userModel;
    }
    return nil;
}

#pragma mark - API

- (void)loginWithLoginType:(LoginType)type accessToken:(NSString*)accessToken facebookId:(NSString*)facebookId {
    //save
    _type = type;
    [kUserDefaults setObject:accessToken forKey:KEY_ACCESS_TOKEN];
    [kUserDefaults setObject:facebookId forKey:KEY_FACEBOOK_ID];
    [kUserDefaults synchronize];
}

- (void)loginWithNormalUser:(NSDictionary*)body complition:(AccountHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    [request setBodyParam:body];
    //call api
    [[WSURLSessionManager shared] wsLoginWithRequest:request handler:^(NSString *token, NSString *error) {
        if (error) {
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}
- (void)loginWithFacebookUser:(long)facebookID password:(NSString*)facebookToken complition:(AccountHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    //set param for request
    //call api
    [[WSURLSessionManager shared] wsLoginWithRequest:request handler:^(NSString *token, NSString *error) {
        if (error) {
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}
- (void)registerWithNormalUserModel:(PPUserModel*)userModel password:(NSString*)password cityId:(int)cityId complition:(AccountHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    NSMutableDictionary *body = [NSMutableDictionary new];
    NSDictionary *user = [MTLJSONAdapter JSONDictionaryFromModel:userModel error:nil];
    [body setObject:user forKey:@"User"];
    [body setObject:password forKey:@"Password"];
    if (cityId) {
        [body setObject:[NSNumber numberWithInt:cityId] forKey:@"CityId"];
    } else {
        [body setObject:[NSNull null] forKey:@"CityId"];
    }
    [request setBodyParam:body];
    //set param for request
    //call api
    [[WSURLSessionManager shared] wsRegisterWithRequest:request handler:^(NSString *token, NSString *error) {
        if (error) {
            NSLog(@"Error: %@",error);
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}
- (void)registerWithFacebookID:(NSInteger)facebookID facebookToken:(NSString*)facebookToken complition:(AccountHandler)handler {
    WSRequest *request = [[WSRequest alloc] init];
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (facebookID) {
        [body setObject:[NSNumber numberWithInteger:facebookID] forKey:@"FacebookId"];
    }
    if (facebookToken) {
        [body setObject:facebookToken forKey:@"FacebookToken"];
    }
    [request setBodyParam:body];
    //call api
    [[WSURLSessionManager shared] wsRegisterWithRequest:request handler:^(NSString *token, NSString *error) {
        if (error) {
            NSLog(@"Error: %@",error);
            if (handler) {
                handler(false, error);
            }
        } else {
            if (handler) {
                handler(true, nil);
            }
        }
    }];
}

- (void)logOut {
    if ([kUserDefaults objectForKey:KEY_ACCESS_TOKEN]) {
        [kUserDefaults removeObjectForKey:KEY_ACCESS_TOKEN];
    }
    if ([kUserDefaults objectForKey:KEY_USER_AVATAR]) {
        [kUserDefaults removeObjectForKey:KEY_USER_AVATAR];
    }
    if ([kUserDefaults objectForKey:KEY_FACEBOOK_ID]) {
        [kUserDefaults removeObjectForKey:KEY_FACEBOOK_ID];
    }
    if ([kUserDefaults objectForKey:KEY_PROFILE_EXIST]) {
        [kUserDefaults removeObjectForKey:KEY_PROFILE_EXIST];
    }
    [kUserDefaults synchronize];
}

@end
