//
//  PPUserManager.h
//  Pipol
//
//  Created by HiTechLtd on 3/21/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDefine.h"
@class PPUserModel;

typedef void (^UserHandler)(BOOL success, NSString* error);
typedef void (^UserObjectHandler)(NSObject *object, NSString* error);
typedef void (^UserDataArrayHandler)(NSArray *result, NSString* error);
@interface PPUserManager : NSObject

+(instancetype)shared;

- (void)getProfileWithComplition:(UserObjectHandler)handler;
- (void)updateProfileWithUser:(PPUserModel*)userModel cityID:(NSInteger)cityID oldPass:(NSString*)oldPass newPass:(NSString*)newPass complition:(UserObjectHandler)handler;
- (void)getMyBalanceWithComplition:(UserDataArrayHandler)handler;
- (void)recallMoneyWithEmail:(NSString*)email amount:(float)amount complition:(UserHandler)handler;
- (void)uploadImageWithImage:(UIImage*)image imageName:(NSString*)imageName complition:(UserHandler)handler;
@end
