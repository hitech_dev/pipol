//
//  PPCityManager.m
//  Pipol
//
//  Created by HiTechLtd on 3/31/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPCityManager.h"
#import "WSRequest.h"
#import "WSURLSessionManager+City.h"

@implementation PPCityManager

+(instancetype)shared {
    static PPCityManager *instance = nil;
    static dispatch_once_t oneTOken;
    dispatch_once(&oneTOken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)getListCityWithID:(NSNumber*)cityID provinceID:(NSNumber*)provinceID complition:(CityDataArrayHandler)handler {
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (cityID != nil) {
        [body setObject:cityID forKey:@"Id"];
    }
    if (provinceID != nil) {
        [body setObject:provinceID forKey:@"ProvinceId"];
    }
    WSRequest *request = [[WSRequest alloc] init];
    [request setBodyParam:body];
    [[WSURLSessionManager shared] wsGetListCityWithRequest:request complition:^(NSArray *result, NSString *error) {
        if (error) {
            if (handler) {
                handler(nil, error);
            }
        } else {
            if (handler) {
                handler(result, nil);
            }
        }
    }];
}

- (void)getListProvinceWithID:(NSNumber*)provinceID countryID:(NSNumber*)countryID complition:(CityDataArrayHandler)handler {
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (provinceID != nil) {
        [body setObject:provinceID forKey:@"Id"];
    }
    if (countryID != nil) {
        [body setObject:countryID forKey:@"CountryId"];
    }
    WSRequest *request = [[WSRequest alloc] init];
    [request setBodyParam:body];
    [[WSURLSessionManager shared] wsGetListProvinceWithRequest:request complition:^(NSArray *result, NSString *error) {
        if (error) {
            if (handler) {
                handler(nil, error);
            }
        } else {
            if (handler) {
                handler(result, nil);
            }
        }
    }];
}

- (void)getListCountryWithID:(NSNumber*)countryID complition:(CityDataArrayHandler)handler {
    NSMutableDictionary *body = [NSMutableDictionary new];
    if (countryID != nil) {
        [body setObject:countryID forKey:@"Id"];
    }
    WSRequest *request = [[WSRequest alloc] init];
    [request setBodyParam:body];
    [[WSURLSessionManager shared] wsGetListCountryWithRequest:request complition:^(NSArray *result, NSString *error) {
        if (error) {
            if (handler) {
                handler(nil, error);
            }
        } else {
            if (handler) {
                handler(result, nil);
            }
        }
    }];
}

@end
