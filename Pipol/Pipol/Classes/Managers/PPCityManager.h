//
//  PPCityManager.h
//  Pipol
//
//  Created by HiTechLtd on 3/31/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDefine.h"

typedef void (^CityDataArrayHandler)(NSArray *result, NSString* error);
@interface PPCityManager : NSObject

+(instancetype)shared;

- (void)getListCityWithID:(NSNumber*)cityID provinceID:(NSNumber*)provinceID complition:(CityDataArrayHandler)handler;
- (void)getListProvinceWithID:(NSNumber*)provinceID countryID:(NSNumber*)countryID complition:(CityDataArrayHandler)handler;
- (void)getListCountryWithID:(NSNumber*)countryID complition:(CityDataArrayHandler)handler;
@end