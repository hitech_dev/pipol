//
//  LoginManager.h
//  Pipol
//
//  Created by HiTechLtd on 3/3/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDefine.h"
@class PPUserModel;

typedef void (^AccountHandler)(BOOL success, NSString *error);
typedef void (^AccountObjectHandler)(NSObject *object, NSString* error);

@interface LoginManager : NSObject

+(instancetype)shared;
- (BOOL)isLoggedIn;
- (LoginType)loginType;
- (PPUserModel*)modelUserProfile;

- (void)loginWithNormalUser:(NSDictionary*)body complition:(AccountHandler)handler;
- (void)loginWithFacebookUser:(long)facebookID password:(NSString*)facebookToken complition:(AccountHandler)handler;
- (void)registerWithNormalUserModel:(PPUserModel*)userModel password:(NSString*)password cityId:(int)cityId complition:(AccountHandler)handler;
- (void)registerWithFacebookID:(NSInteger)facebookID facebookToken:(NSString*)facebookToken complition:(AccountHandler)handler;
- (void)logOut;

@end
