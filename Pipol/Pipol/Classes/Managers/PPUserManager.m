//
//  PPUserManager.m
//  Pipol
//
//  Created by HiTechLtd on 3/21/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "PPUserManager.h"
#import "AppDefine.h"
#import "WSRequest.h"
#import "WSURLSessionManager+User.h"
#import "WSURLSessionManager+Job.h"
#import "PPUserModel.h"
#import "NSString+Help.h"
#import "PPBalanceModel.h"
#import "PPCityModel.h"

@implementation PPUserManager

+(instancetype)shared {
    static PPUserManager *instance = nil;
    static dispatch_once_t oneTOken;
    dispatch_once(&oneTOken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)getProfileWithComplition:(UserObjectHandler)handler {
    if ([kUserDefaults objectForKey:KEY_ACCESS_TOKEN]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:[kUserDefaults objectForKey:KEY_ACCESS_TOKEN] forKey:@"Token"];
        WSRequest *request = [[WSRequest alloc] init];
        [request setBodyParam:body];
        [request setURL: [NSURL URLWithString: WS_USER_GET_PROFILE]];
        [[WSURLSessionManager shared] wsGetProfileWithRequest:request handler:^(NSObject *object, NSString *error) {
            if (error) {
                if (handler) {
                    handler(nil, error);
                }
            } else {
                if (handler) {
                    handler(object, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(nil, @"Not have access token");
        }
    }
}

- (void)updateProfileWithUser:(PPUserModel*)userModel cityID:(NSInteger)cityID oldPass:(NSString*)oldPass newPass:(NSString*)newPass complition:(UserObjectHandler)handler {
    if ([kUserDefaults objectForKey:KEY_ACCESS_TOKEN]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:[kUserDefaults objectForKey:KEY_ACCESS_TOKEN] forKey:@"Token"];
        NSDictionary *user = [MTLJSONAdapter JSONDictionaryFromModel:userModel error:nil];
        if (user) {
            [body setObject:user forKey:@"User"];
        } else {
            [body setObject:[PPUserModel new] forKey:@"User"];
        }
        if (cityID) {
            [body setObject:[NSNumber numberWithInteger:cityID] forKey:@"CityId"];
        }
        if (oldPass && oldPass.isEmpty == false) {
            [body setObject:oldPass forKey:@"OldPassword"];
        } else {
            [body setObject:@"" forKey:@"OldPassword"];
        }
        if (newPass && newPass.isEmpty == false) {
            [body setObject:newPass forKey:@"NewPassword"];
        } else {
            [body setObject:@"" forKey:@"NewPassword"];
        }
        WSRequest *request = [[WSRequest alloc] init];
        [request setBodyParam:body];
        [request setURL: [NSURL URLWithString: WS_USER_UPDATE_PROFILE]];
        [[WSURLSessionManager shared] wsUploadProfileWithRequest:request handler:^(NSObject *object, NSString *error) {
            if (error) {
                if (handler) {
                    handler(nil, error);
                }
            } else {
                if (handler) {
                    handler(object, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(nil, @"Not have access token");
        }
    }
}

- (void)getMyBalanceWithComplition:(UserDataArrayHandler)handler {
    if ([kUserDefaults objectForKey:KEY_ACCESS_TOKEN]) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:[kUserDefaults objectForKey:KEY_ACCESS_TOKEN] forKey:@"Token"];
        WSRequest *request = [[WSRequest alloc] init];
        [request setBodyParam:body];
        [[WSURLSessionManager shared] wsGetMyBalanceWithRequest:request hadler:^(NSArray *result, NSString *error) {
            if (error) {
                if (handler) {
                    handler(nil, error);
                }
            } else {
                if (handler) {
                    handler(result, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(nil, @"Not have access token");
        }
    }
}

- (void)recallMoneyWithEmail:(NSString*)email amount:(float)amount complition:(UserHandler)handler {
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (token && email && amount) {
        NSMutableDictionary *body = [NSMutableDictionary new];
        [body setObject:[kUserDefaults objectForKey:KEY_ACCESS_TOKEN] forKey:@"Token"];
        [body setObject:email forKey:@"Email"];
        [body setObject:[NSNumber numberWithFloat: amount] forKey:@"Amount"];
        WSRequest *request = [[WSRequest alloc] init];
        [request setBodyParam:body];
        [[WSURLSessionManager shared] wsRecallMoneyWithRequest:request hadler:^(BOOL success, NSString *error) {
            if (error) {
                if (handler) {
                    handler(success, error);
                }
            } else {
                if (handler) {
                    handler(success, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(false, @"Not have access token, email or amount");
        }
    }
}

- (void)uploadImageWithImage:(UIImage*)image imageName:(NSString*)imageName complition:(UserHandler)handler {
    NSString *token = [kUserDefaults objectForKey:KEY_ACCESS_TOKEN];
    if (token && image) {
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:token forKey:@"Token"];
        [param setObject:@"File" forKey:@"File"];
        NSString *url = WS_USER_UPLOAD_IMAGE;
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        [[WSURLSessionManager shared] wsUploadImageWith:param serviceUrlPath:url data:imageData name:@"File" fileName:imageName mineType:@"image/png" complition:^(NSObject *object, NSString *error) {
            if (error) {
                if (handler) {
                    handler(nil, error);
                }
            } else {
                if (handler) {
                    handler(object, nil);
                }
            }
        }];
    } else {
        if (handler) {
            handler(nil, @"Token, image or param are invalid!");
        }
    }
 
}

@end
