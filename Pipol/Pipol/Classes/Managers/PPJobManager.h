//
//  PPJobManager.h
//  Pipol
//
//  Created by HiTechLtd on 3/10/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDefine.h"

typedef void (^JobHandler)(BOOL success, NSString* error);
typedef void (^JobDataArrayHandler)(NSArray *result, NSString* error);
typedef void (^JobObjectHandler)(NSObject *object, NSString* error);

@interface PPJobManager : NSObject

+(instancetype)shared;

- (void)getJobsByPositionLatitude:(double)latitude longitude:(double)longitude distance:(double)distance complition:(JobDataArrayHandler)handler;
- (void)getMyJobsWithComplition:(JobDataArrayHandler)handler;
- (void)getJobWithID:(NSInteger)jobID complition:(JobObjectHandler)handler;
- (void)applyJobWithID:(NSInteger)jobID complition:(JobHandler)handler;
- (void)uploadImageWithID:(NSInteger)jobID image:(UIImage*)image imageName:(NSString*)imageName complition:(JobObjectHandler)handler;
- (void)uploadAudioWithJobID:(NSInteger)jobID audioPath:(NSString*)audioPath audioName:(NSString*)audioName  complition:(JobObjectHandler)handler;
- (void)answerQuizWithQuizID:(NSInteger)quizID answer:(NSString*)answer complition:(JobHandler)handler;
- (void)finishJobWithJobID:(NSInteger)jobID complition:(JobHandler)handler;
@end
