//
//  MainViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"	

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.title = @"Main";
    [self defaultRevealViewControllerOnNavigation];
    NSMutableDictionary *diction = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor blackColor], NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = diction;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    

    
}

- (void)defaultRevealViewControllerOnNavigation {
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu.png"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.revealViewController revealToggleAnimated:YES];
    }
}

@end
