//
//  RegisterViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class PPUserModel;

@interface RegisterViewController : BaseViewController

@property(nonatomic, strong) PPUserModel *userModel;
@end
