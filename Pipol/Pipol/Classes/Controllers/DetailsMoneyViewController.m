//
//  DetailsMoneyViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "DetailsMoneyViewController.h"
#import <PureLayout/PureLayout.h>
#import "AcceptDetailsMoneyView.h"
#import "AppDefine.h"
#import "UIHelpers.h"
#import "PPUserManager.h"
#import "NSString+Help.h"
#import "LeftPaddingTextField.h"

@interface DetailsMoneyViewController ()<AcceptDetailsMoneyViewDelegate> {
    __weak AcceptDetailsMoneyView *_acceptView;
    __weak LeftPaddingTextField *currentTextField;
}

@end

@implementation DetailsMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self setUpTextFields];
}

- (void)_setupNavigationBar {
    self.title = @"Retirar Dinero";
    [self customBackButton];
}

- (void)setUpTextFields {
    self.tfEmail.keyboardType = UIKeyboardTypeEmailAddress;
    self.tfMoney.keyboardType = UIKeyboardTypeNumberPad;
}

- (void)__showAcceptView {
    if (_acceptView == nil) {
        AcceptDetailsMoneyView *acceptView = [AcceptDetailsMoneyView nibView];
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview:acceptView];
        acceptView.translatesAutoresizingMaskIntoConstraints = NO;
        [acceptView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        _acceptView = acceptView;
        _acceptView.delegate = self;
    }
}

#pragma mark: - verify
- (BOOL)checkVerify {
    if ([self.tfMoney.text isEmpty]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Money.") delegate:self tag:0];
        currentTextField = self.tfMoney;
        [self.tfMoney becomeFirstResponder];
        return NO;
    } else if ([self.tfMoney.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location != NSNotFound) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Money no es válida.") delegate:self tag:0];
        currentTextField = self.tfMoney;
        [self.tfMoney becomeFirstResponder];
        return NO;
    } else if ([self.tfEmail.text isEmpty]) {
        currentTextField = self.tfEmail;
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Email.") delegate:self tag:0];
        [self.tfEmail becomeFirstResponder];
        return NO;
    } else if (![self.tfEmail.text isValidEmail]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Email no es válida.") delegate:self tag:0];
        currentTextField = self.tfEmail;
        [self.tfEmail becomeFirstResponder];
        return NO;
    } else if ([self.tfRepetirEmail.text isEmpty]) {
        currentTextField = self.tfRepetirEmail;
        [self.tfRepetirEmail becomeFirstResponder];
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Repetir Email.") delegate:self tag:0];
        return NO;
    } else if (![self.tfRepetirEmail.text isValidEmail]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Repetir Email no es válida.") delegate:self tag:0];
        currentTextField = self.tfRepetirEmail;
        [self.tfRepetirEmail becomeFirstResponder];
        return NO;
    } else if (![self.tfEmail.text isEqualToString:self.tfRepetirEmail.text]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Repetir Email y Email no coincide") delegate:self tag:0];
        currentTextField = self.tfRepetirEmail;
        [self.tfRepetirEmail becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (IBAction)__actionRetire:(id)sender {
    if ([self checkVerify]) {
        [self __showAcceptView];
    }
}

- (void)view:(AcceptDetailsMoneyView *)view acionAccept:(UIButton *)sender {
    [_acceptView removeFromSuperview];
    [self callAPIRecallMoney];
}

#pragma mark: - API recall money
- (void)callAPIRecallMoney {
    [UIHelpers showLoadingView:kWindow];
    [[PPUserManager shared] recallMoneyWithEmail: self.tfEmail.text amount:[self.tfMoney.text floatValue] complition:^(BOOL success, NSString *error) {
        [UIHelpers hideLoadingView];
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:@"Success" delegate:self tag:1];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}
@end
