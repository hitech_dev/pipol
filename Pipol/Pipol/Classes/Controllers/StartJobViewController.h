//
//  StartJobViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class PPJobModel;

@interface StartJobViewController : BaseViewController

@property (nonatomic, strong) PPJobModel *jobModel;

@property (nonatomic, strong) UIImageView *imageViewTMP;
@end
