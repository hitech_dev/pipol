//
//  LoginViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "LoginViewController.h"
#import "RoundButton.h"
#import "RegisterViewController.h"
#import "NSObject+NSObjectExtension.h"
#import "SWRevealViewController.h"
#import "LoginManager.h"
#import "AppDefine.h"
#import "InputWhiteTextField.h"
#import "PPUserModel.h"
#import "UIHelpers.h"
#import "NSString+Help.h"
#import "PPUserManager.h"
#import "MyAccountViewController.h"
#import "BaseNavigationController.h"
#import "TermsAndConditionsView.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <PureLayout/PureLayout.h>
#import "EAIntroView.h"


@interface LoginViewController ()<InputWhiteTextFieldDelegate,TermsAndConditionsViewDelegate,EAIntroDelegate> {
    __weak IBOutlet InputWhiteTextField *tfEmail;
    __weak IBOutlet InputWhiteTextField *tfPassword;
    __weak IBOutlet RoundButton *registerButton;
    __weak IBOutlet RoundButton *facebookButton;
    __weak IBOutlet RoundButton *loginButton;
    PPUserModel *userModel;
    __weak UITextField *currentTextField;
    __weak TermsAndConditionsView *_termsView;
}

@end

@implementation LoginViewController

#pragma mark: - SetUps

- (void)viewDidLoad{
    if (![[[NSUserDefaults standardUserDefaults] stringForKey:IS_SHOW_TUTORIAL_PIPOL] isEqualToString:@"YES"]) {
        [self setupTutorialPipol];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupNavigationBar];
    [self setUIPasswordTextField];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateLayerButtons];
}

- (void)setupNavigationBar {
    self.navigationController.navigationBar.topItem.title = @"Log in";
    [self setupWhiteBarOfNavigation];
}

- (void)setUIPasswordTextField {
    [tfPassword showRightImage:nil];
    tfPassword.inputDelegate = self;
}

- (void)updateLayerButtons {
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    registerButton.layer.cornerRadius = registerButton.bounds.size.height/2;
    facebookButton.layer.cornerRadius = facebookButton.bounds.size.height/2;
    loginButton.layer.cornerRadius = loginButton.bounds.size.height/2;
}

- (void)handleSingleTap {
    [self.view endEditing:YES];
}

#pragma mark - Setup Tutorial App
- (void)setupTutorialPipol {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:IS_SHOW_TUTORIAL_PIPOL];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView1.image = [UIImage imageNamed:@"presentacion_curvas-1.png"];
    EAIntroPage *page1 = [EAIntroPage pageWithCustomView:imageView1];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView2.image = [UIImage imageNamed:@"presentacion_curvas-2.png"];
    EAIntroPage *page2 = [EAIntroPage pageWithCustomView:imageView2];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView3.image = [UIImage imageNamed:@"presentacion_curvas-3.png"];
    EAIntroPage *page3 = [EAIntroPage pageWithCustomView:imageView3];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView4.image = [UIImage imageNamed:@"presentacion_curvas-4.png"];
    EAIntroPage *page4 = [EAIntroPage pageWithCustomView:imageView4];
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView5.image = [UIImage imageNamed:@"presentacion_curvas-5.png"];
    EAIntroPage *page5 = [EAIntroPage pageWithCustomView:imageView5];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:[UIScreen mainScreen].bounds andPages:@[page1,page2,page3,page4,page5]];
    [intro.skipButton setTitle:@"Skip now" forState:UIControlStateNormal];
    [intro setDelegate:self];
    [intro showInView:kWindow animateDuration:0.0];
}

#pragma mark - EAIntroView delegate
- (void)introDidFinish:(EAIntroView *)introView {
    NSLog(@"introDidFinish callback");
}

#pragma mark - TermsAndConditionsViewDelegate
- (void)acceptedTerms{
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:IS_ACCEPT_TERMS_PIPOL];
    [self callAPILoginNormalUser];
}

#pragma mark: - actions
- (IBAction)__actionRegister:(id)sender {
    [self gotoRegister];
}

- (IBAction)__actionLoginFacebook:(id)sender {
    [self loginWithFacebook];
}

- (void)commingApp {
    MyAccountViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[MyAccountViewController identifier]];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = nav;
    [window makeKeyAndVisible];
}

- (IBAction)__actionLoginEmail:(id)sender {
    [self.view endEditing:YES];
    if ([self checkVerify]) {
        
        if (![[[NSUserDefaults standardUserDefaults] stringForKey:IS_ACCEPT_TERMS_PIPOL] isEqualToString:@"YES"]) {
            if (_termsView == nil) {
                TermsAndConditionsView *startView = [TermsAndConditionsView nibView];
                [kWindow addSubview:startView];
                startView.translatesAutoresizingMaskIntoConstraints = NO;
                [startView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
                _termsView = startView;
                _termsView.delegate = self;
            }
        }else{
            [self callAPILoginNormalUser];
        }
    }
}

- (void)gotoRegister {
    RegisterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[RegisterViewController identifier]];
    vc.userModel = userModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSDictionary*)bodyForLoginWithNormalUser {
    NSMutableDictionary *body = [NSMutableDictionary new];
    body[@"Username"] = tfEmail.text;
    body[@"Password"] = tfPassword.text;
    return body;
}
- (void)callAPILoginNormalUser {
    NSDictionary *body = [self bodyForLoginWithNormalUser];
    [UIHelpers showLoadingView:kWindow];
    [[LoginManager shared] loginWithNormalUser:body complition:^(BOOL success, NSString *error) {
        if (success) {
            [kUserDefaults setObject:tfPassword.text forKey: KEY_PASSWORD_EXIST];
            [kUserDefaults synchronize];
            [self commingApp];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:0];
        }
        [UIHelpers hideLoadingView];
    }];
}

- (void)callAPILoginFacebookUserWithID:(NSInteger)faceID token:(NSString*)faceToken {
    [UIHelpers showLoadingView:kWindow];
    [[LoginManager shared] registerWithFacebookID:faceID facebookToken:faceToken complition:^(BOOL success, NSString *error) {
        if (success) {
            [self commingApp];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:0];
        }
        [UIHelpers hideLoadingView];
    }];
}

- (BOOL)checkVerify {
    if ([tfEmail.text isEmpty]) {
        currentTextField = tfEmail;
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Email.") delegate:self tag:0];
        return NO;
    } else if (![tfEmail.text isValidEmail]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Email no es válida.") delegate:self tag:0];
        currentTextField = tfEmail;
        return NO;
    } else if ([tfPassword.text isEmpty]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Passsword.") delegate:self tag:0];
        currentTextField = tfPassword;
        return NO;
    } else if ([tfPassword.text isCheckWhitleSpace]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Passsword no es válida.") delegate:self tag:0];
        currentTextField = tfPassword;
        return NO;
    }
    return YES;
}

#pragma mark: Login With Facebook
- (void)loginWithFacebook {
    if (![kUserDefaults objectForKey:KEY_ACCESS_TOKEN]) {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error)
            {
                NSLog(@"Error");
            }
            else if (result.isCancelled)
            {
                NSLog(@"Cancell");
            } else {
                NSLog(@"Login Sucessfull");
                if ([FBSDKAccessToken currentAccessToken]) {
                    NSLog(@"Access Token:  %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
                    
                }
                if ([result.grantedPermissions containsObject:@"public_profile"])
                {
                    NSLog(@"result is:%@",result);
                    
                    [self fetchUserInfo];
                }
            }
        }];
    }
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), cover, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"resultis:%@",result);
                 if (result && [result isKindOfClass:[NSDictionary class]]) {
                     userModel = [PPUserModel new];
                     userModel.userFacebookId = [result[@"id"] doubleValue];
                     userModel.userEmail = result[@"email"];
                     NSDictionary *picDiction = result[@"picture"];
                     NSDictionary *dataDiction = picDiction[@"data"];
                     if (dataDiction[@"url"] != [NSNull null]) {
                         userModel.userImage = dataDiction[@"url"];
                         [kUserDefaults setObject:dataDiction[@"url"] forKey:KEY_USER_AVATAR];
                         [kUserDefaults synchronize];
                     }
                     NSDictionary *coverDiction = result[@"cover"];
                     NSString *coverString = coverDiction[@"source"];
                     if (coverString != nil) {
                         [kUserDefaults setObject:coverString forKey:KEY_USER_COVER_IMAGE];
                         [kUserDefaults synchronize];
                     }
                     userModel.userName = result[@"name"];
                     [self callAPILoginFacebookUser];
//                     [self callAPILoginFacebookUserWithID:(NSInteger)userModel.userFacebookId token:[[FBSDKAccessToken currentAccessToken]tokenString]];
                 }
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
    }
}

- (void)callAPIUploadUserProfileWithModel:(PPUserModel*)model {
    [UIHelpers showLoadingView:kWindow];
    [[PPUserManager shared] updateProfileWithUser: model cityID:0 oldPass:nil newPass:nil complition:^(NSObject *object, NSString *error) {
        [UIHelpers hideLoadingView];
        if (!error && [object isKindOfClass:[PPUserModel class]]) {
            [kMainQueue addOperationWithBlock:^{
                NSDictionary *diction = [MTLJSONAdapter JSONDictionaryFromModel:(PPUserModel*)object error:nil];
                NSData *userData = [NSJSONSerialization dataWithJSONObject:diction options:0 error:nil];
                if ([kUserDefaults objectForKey:KEY_PROFILE_EXIST]) {
                    [kUserDefaults removeObjectForKey:KEY_PROFILE_EXIST];
                }
                [kUserDefaults setObject:userData forKey:KEY_PROFILE_EXIST];
                [kUserDefaults synchronize];
                [self commingApp];
            }];
        }
    }];
}

#pragma register facebook
- (NSDictionary*)bodyForLoginWithFacebookUserEmail:(NSString*)email pass:(NSString*)password {
    NSMutableDictionary *body = [NSMutableDictionary new];
    body[@"Username"] = email;
    body[@"Password"] = password;
    return body;
}
- (void)callAPILoginFacebookUser {
    NSDictionary *body = [self bodyForLoginWithFacebookUserEmail:userModel.userEmail pass:@"99999"];
    [UIHelpers showLoadingView:kWindow];
    [[LoginManager shared] loginWithNormalUser:body complition:^(BOOL success, NSString *error) {
        if (success) {
            [kUserDefaults setObject:tfPassword.text forKey: KEY_PASSWORD_EXIST];
            [kUserDefaults synchronize];
            [self commingApp];
        } else {
            [self callAPIRegister];
        }
        [UIHelpers hideLoadingView];
    }];
}

- (void)callAPIRegister {
    [UIHelpers showLoadingView:kWindow];
    PPUserModel *model = [self userModelToRegister];
    [[LoginManager shared] registerWithNormalUserModel:model password:@"99999" cityId:0 complition:^(BOOL success, NSString *error) {
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:0];
        } else {
            [kUserDefaults setObject:tfPassword.text forKey: KEY_PASSWORD_EXIST];
            [kUserDefaults synchronize];
            [self commingApp];
        }
        [UIHelpers hideLoadingView];
    }];
}

- (PPUserModel*)userModelToRegister {
    PPUserModel *model = [PPUserModel new];
    model.userName = userModel.userName;
    model.userAddress = @"";
    model.userDNI = (int)userModel.userFacebookId;
    model.userEmail = userModel.userEmail;
    model.userPhone = userModel.userPhone;
    model.userFacebookId = userModel.userFacebookId;
    model.userId = 0;
    model.userImage = userModel.userImage;
    return model;
}

#pragma mark: - Input textField Delegate
- (void)actionRightViewAtTextField:(InputWhiteTextField *)textField {
    
}

#pragma mark: alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [currentTextField becomeFirstResponder];
}
@end
