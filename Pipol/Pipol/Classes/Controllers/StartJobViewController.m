//
//  StartJobViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "StartJobViewController.h"
#import "StartJobCell.h"
#import "UIView+UIViewExtension.h"
#import "UIViewController+UIViewControllerExtension.h"
#import "ResponderViewController.h"
#import "ListImageView.h"
#import <PureLayout/PureLayout.h>
#import "PPJobModel.h"
#import "PPJobBranchModel.h"
#import "PPQuizModel.h"
#import "AppDefine.h"
#import "UIHelpers.h"
#import "NSString+Help.h"
#import "UIImageView+AFNetworking.h"
#import "PPJobManager.h"
#import <AVFoundation/AVFoundation.h>
#import "AudioSessionManager.h"
#import "ImagePickerViewController.h"
#import "PPImageModel.h"
#import "ProblemViewController.h"
#import "QuizViewController.h"

#define height_start_cell 84*ScreenScale
#define IMPEDE_PLAYBACK NO

@interface StartJobViewController ()<UITableViewDataSource, UITableViewDelegate, ListImageViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate> {
    NSArray *_dataTable;
    
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet NSLayoutConstraint *heightTableView;
    __weak IBOutlet UIView *listImageView;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UILabel *lblAddress;
    __weak IBOutlet UILabel *lblTiempoRestante;
    __weak IBOutlet UIImageView *imgLogo;
    __weak IBOutlet UILabel *lblTime;
    __weak IBOutlet UIButton *btnRecord;
    __weak IBOutlet UIButton *btnPlayAndPause;
    __weak IBOutlet UIButton *btnUploadAudio;
    __weak IBOutlet UIImageView *imvImageJob1;
    __weak IBOutlet UIImageView *imvImageJob2;
    __weak IBOutlet UIImageView *imvImageJob3;
    
    ListImageView *localListView;
    UIAlertController *alertController;
    UIImagePickerController *pickerController;
   
    int indexSelectedImage;
    NSMutableArray *selectedImageArray;
    int timeCount;
    NSTimer *timer;
    BOOL recording;
    
    AVAudioPlayer *player;
    
    AVAudioRecorder     *recorder;
    NSString            *recordedAudioFileName;
    NSURL               *recordedAudioURL;
}



@end

@implementation StartJobViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self _setupUI];
    [self _setupTableView];
    [self _setupListImageView];
    [self _loadData];
    [self setupAndPrepareToRecord];
    selectedImageArray = [[NSMutableArray alloc] init];
}

- (void)setupAndPrepareToRecord
{
    if (!IMPEDE_PLAYBACK) {
        [AudioSessionManager setAudioSessionCategory:AVAudioSessionCategoryRecord];
    }
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSinceNow:1];
    recordedAudioFileName = [NSString stringWithFormat:@"%@", date];
    // sets the path for audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               [NSString stringWithFormat:@"%@.m4a", recordedAudioFileName],
                               nil];
    recordedAudioURL = [NSURL fileURLWithPathComponents:pathComponents];
    // settings for the recorder
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    // initiate recorder
    NSError *error;
    recorder = [[AVAudioRecorder alloc] initWithURL:recordedAudioURL settings:recordSetting error:&error];
    [recorder prepareToRecord];
}


#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Finish playing the recording!"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [self resetPlayer];
    [timer invalidate];
    timer = nil;
}

- (void)_setupUI {
    [btnRecord.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [btnPlayAndPause.imageView setContentMode:UIViewContentModeScaleAspectFit];
}

- (void)_setupNavigationBar {
    self.title = @"Carrefour Express";
    [self customBackButton];
    [self setupBarButtonSave];
}

- (void)_setupTableView {
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerNib:[UINib nibWithNibName:[StartJobCell identifier] bundle:nil] forCellReuseIdentifier:[StartJobCell identifier]];
    _tableView.rowHeight = height_start_cell;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)_setupListImageView {
    ListImageView *view = [[NSBundle mainBundle] loadNibNamed:@"ListImageView" owner:nil options:nil][0];
    [listImageView addSubview:view];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [view autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    view.delegate = self;
    localListView = view;
}

- (void)_loadData {
    [self _tempData];
    heightTableView.constant = _dataTable.count * height_start_cell;
    if (self.jobModel) {
        [kMainQueue addOperationWithBlock:^{
            lblName.text = self.jobModel.jobBranch.branchName;
            lblAddress.text = self.jobModel.jobBranch.branchAddress;
            lblTiempoRestante.text = [@"TIEMPO RESTANTE: " stringByAppendingString: [UIHelpers stringTimeFromDate:self.jobModel.jobDueDate intoDate:self.jobModel.jobApplyDate]];
            if (_jobModel.jobImage) {
                [imgLogo setImageWithURL:[NSURL URLWithString:[URL_IMAGE_JOB stringByAppendingString: _jobModel.jobImage]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"]];
            }
            
            if (self.jobModel.jobImages.count > 0) {
                PPImageModel *obj = [self.jobModel.jobImages objectAtIndex:self.jobModel.jobImages.count-1];
                NSString *strImageName = [NSString stringWithFormat:@"%@%@",URL_IMAGE_JOB,obj.imgFile];
                [imvImageJob1 setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strImageName]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                    [localListView refreshImage:image atIndex:0];
                } failure:nil];
            }
            
            if (self.jobModel.jobImages.count > 1) {
                PPImageModel *obj = [self.jobModel.jobImages objectAtIndex:self.jobModel.jobImages.count-2];
                NSString *strImageName = [NSString stringWithFormat:@"%@%@",URL_IMAGE_JOB,obj.imgFile];
                [imvImageJob2 setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strImageName]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                    [localListView refreshImage:image atIndex:1];
                } failure:nil];
            }
            
            if (self.jobModel.jobImages.count > 2) {
                PPImageModel *obj = [self.jobModel.jobImages objectAtIndex:self.jobModel.jobImages.count-3];
                NSString *strImageName = [NSString stringWithFormat:@"%@%@",URL_IMAGE_JOB,obj.imgFile];
                [imvImageJob3 setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strImageName]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                    [localListView refreshImage:image atIndex:2];
                } failure:nil];
            }
        }];
    }
}

- (void)_tempData {
    _dataTable = self.jobModel.jobQuizzes;
}
#pragma mark: - actions
- (IBAction)actionBottomGuardar:(id)sender {
    
}

- (IBAction)actionBottomCancelar:(id)sender {
    ProblemViewController *vc = [[ProblemViewController alloc] initWithNibName:[ProblemViewController identifier] bundle:nil];
//    QuizViewController *vc = [[QuizViewController alloc] initWithNibName:[QuizViewController identifier] bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)actionSave:(UIButton *)sender {
    [super actionSave:sender];
    [self callAPIFinishJob];
}

- (IBAction)actionTapRecord:(id)sender {
    if (!recorder.recording) {
        [recorder record];
        [self startTimer];
        recording = YES;
        [btnRecord setImage:[UIImage imageNamed:@"ic_stop_record.png"] forState:UIControlStateNormal];
    } else {
        // Pause recording
        [recorder stop];
        [timer invalidate];
        timer = nil;
        recording = NO;
        [btnRecord setImage:[UIImage imageNamed:@"ic_micro.png"] forState:UIControlStateNormal];
        btnUploadAudio.enabled = YES;
        btnPlayAndPause.enabled = YES;
    }
}

- (IBAction)actionUpdateRecord:(id)sender {
    NSString *audioName = [[NSString stringWithFormat:@"%tu", _jobModel.jobId] stringByAppendingString:@"_Audio"];
    NSLog(@"%@-%@",recordedAudioFileName,recordedAudioURL);
    [UIHelpers showLoadingView:kWindow];
    [[PPJobManager shared] uploadAudioWithJobID:_jobModel.jobId audioPath:recordedAudioURL.path audioName:[NSString stringWithFormat:@"%@.mp3",audioName] complition:^(NSObject *object, NSString *error) {
        [UIHelpers hideLoadingView];
        if (!error) {
            [UIHelpers showAlertViewErrorWithMessage:@"exitosamente" delegate:self tag:10];
        }
    }];
}

- (IBAction)actionPlayAndPauseAudio:(id)sender {
    if (player == nil) {
        
        if (!IMPEDE_PLAYBACK) {
            [AudioSessionManager setAudioSessionCategory:AVAudioSessionCategoryPlayback];
        }
        
        if (!btnUploadAudio.enabled) {
            

            
            id obj = [self.jobModel.jobImages objectAtIndex:self.jobModel.jobAudios.count-1];
            
//            NSURL *playURL = [NSURL URLWithString:playString];
        }
        
        
        

        NSError *audioError;
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recordedAudioURL error:&audioError];
        player.delegate = self;
        [player play];
        
        [self startTimer];
        [btnPlayAndPause setImage:[UIImage imageNamed:@"ic_pause_audio.png"] forState:UIControlStateNormal];
    } else {
        [player stop];
        [timer invalidate];
        timer = nil;
        player = nil;
        [btnPlayAndPause setImage:[UIImage imageNamed:@"ic_play_audio.png"] forState:UIControlStateNormal];
    }
    
}

- (void)resetPlayer {
    [player stop];
    [timer invalidate];
    timer = nil;
    player = nil;
    [btnPlayAndPause setImage:[UIImage imageNamed:@"ic_play_audio.png"] forState:UIControlStateNormal];
}

- (void)startTimer {
    timeCount = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
}

#pragma mark: refreshs
- (void)refreshTimer {
    timeCount++;
    [self refreshTimeLabel];
}

- (void)refreshTimeLabel {
    int minute = timeCount / 60;
    int second = timeCount % 60;
    NSString *strMinute;
    NSString *strSecond;
    if (minute < 10) {
        strMinute = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", minute]];
    } else {
        strMinute = [NSString stringWithFormat:@"%d", minute];
    }
    if (second < 10) {
        strSecond = [@"0" stringByAppendingString:[NSString stringWithFormat:@"%d", second]];
    } else {
        strSecond = [NSString stringWithFormat:@"%d", second];
    }
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        lblTime.text = [[strMinute stringByAppendingString:@":"] stringByAppendingString:strSecond];
    }];
}
#pragma mark: api
- (void)callAPIFinishJob {
    [UIHelpers showLoadingView:kWindow];
//    for (int i = 0; i < selectedImageArray.count; i++) {
//        UIImage *image = selectedImageArray[i];
//        NSString *imageName = [[[NSString stringWithFormat:@"%tu", _jobModel.jobId] stringByAppendingString:@"_JobImage_"] stringByAppendingString:[NSString stringWithFormat:@"%tu", i]];
//        [[PPJobManager shared] uploadImageWithID:_jobModel.jobId image:image imageName:imageName complition:^(NSObject *object, NSString *error) {
//            
//        }];
//    }
    
    [[PPJobManager shared] finishJobWithJobID:_jobModel.jobId complition:^(BOOL success, NSString *error) {
        [UIHelpers hideLoadingView];
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:@"Success!!!" delegate:self tag:2];
        }
    }];
}

#pragma mark - TableView delegate, datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataTable.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StartJobCell *cell = [tableView dequeueReusableCellWithIdentifier:[StartJobCell identifier] forIndexPath:indexPath];
    if (_dataTable && indexPath.row < _dataTable.count) {
        PPQuizModel *quizModel = [_dataTable objectAtIndex:indexPath.row];
        cell.indexPath = indexPath;
        cell.quizModel = quizModel;
        
    }
    //    if (indexPath.row < _dataTable.count - 1) {
    //        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    } else {
    //        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    //    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return height_start_cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    ResponderViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[ResponderViewController identifier]];
    QuizViewController *vc = [[QuizViewController alloc] initWithNibName:[QuizViewController identifier] bundle:nil];
    vc.order = indexPath.row;
    vc.quizModel = [_dataTable objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark: - list image delegate
-  (void)selectImageAtIndex:(int)index {
    indexSelectedImage = index;
    ImagePickerViewController *imagePicker = [[ImagePickerViewController alloc] initAtController:self];
    imagePicker.callBack = ^(UIImage *image) {
        [localListView refreshImage:image atIndex:indexSelectedImage];
        [selectedImageArray addObject:image];
        
        NSString *imageName = [NSString stringWithFormat:@"%d_JobImage_%d.png",_jobModel.jobId,index];
        [[PPJobManager shared] uploadImageWithID:_jobModel.jobId image:image imageName:imageName complition:nil];
    };
}

- (void)removeImageAtIndex:(int)index {
    if (selectedImageArray && selectedImageArray.count > index) {
        [selectedImageArray removeObjectAtIndex:index];
    }
}

- (NSString *)stringDate {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate new]];
    NSRange colonRange = [dateString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@":"] options:NSBackwardsSearch];
    return [dateString stringByReplacingCharactersInRange:colonRange withString:@""];
}

@end
