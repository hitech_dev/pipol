//
//  QuizViewController.m
//  Pipol
//
//  Created by HitechLtd on 4/19/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "QuizViewController.h"
#import <DLRadioButton/DLRadioButton.h>
#import "PPQuizModel.h"
#import "PPJobModel.h"
#import "PPJobManager.h"
#import "UIHelpers.h"

@interface QuizViewController ()
{
    __weak IBOutlet DLRadioButton *btnAnswer1;
    __weak IBOutlet DLRadioButton *btnAnswer2;
    __weak IBOutlet DLRadioButton *btnAnswer3;
    __weak IBOutlet DLRadioButton *btnAnswer4;
    
    NSString *answer;
}
@end

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self fillData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)_setupNavigationBar {
    self.title = @"Responder";
    [self customBackButton];
    [self setupBarButtonSave];
}

- (void)fillData {
    
    NSArray *arrayAnswer = [NSArray arrayWithObjects:@"Los veo bien, buen gusto.",@"Deberian estar mejor acomodados.",@"Yo cambiaria algunas cosas por por ejemplo los detalles.",@"No cambiaria nada.", nil];
    
    self.lblOrder.text = [NSString stringWithFormat:@"%ld",(long)self.order+1];
    self.tvQuestion.text = self.quizModel.quizContent;
    [self.view layoutIfNeeded];
    
    if ([arrayAnswer containsObject:self.quizModel.quizAnswer]) {
        NSInteger indexAnswer = [arrayAnswer indexOfObject:self.quizModel.quizAnswer];
        switch (indexAnswer) {
            case 0:
                [btnAnswer1 setSelected:YES];;
                break;
            case 1:
                [btnAnswer2 setSelected:YES];;
                break;
            case 2:
                [btnAnswer3 setSelected:YES];;
                break;
            case 3:
                [btnAnswer4 setSelected:YES];;
                break;
                
            default:
                break;
        }
    }
    
}

- (void)actionSave:(UIButton *)sender {
    [super actionSave:sender];
    [self callAPIAnswer];
}

- (void)callAPIAnswer {
    [UIHelpers showLoadingView:kWindow];
    [[PPJobManager shared] answerQuizWithQuizID:self.quizModel.quizId answer:answer complition:^(BOOL success, NSString *error) {
        [UIHelpers hideLoadingView];
        if (success) {
            self.quizModel.quizAnswer = answer;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        }
    }];
}

- (IBAction)logSelectedButton:(DLRadioButton *)radioButton {
    if (radioButton.isMultipleSelectionEnabled) {
        for (DLRadioButton *button in radioButton.selectedButtons) {
            NSLog(@"%@ is selected.\n", button.titleLabel.text);
        }
    } else {
        NSLog(@"%@ is selected.\n", radioButton.selectedButton.titleLabel.text);
        answer = radioButton.selectedButton.titleLabel.text;
    }
}

@end
