//
//  DetailsJobViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "DetailsJobViewController.h"
#import <PureLayout/PureLayout.h>
#import "DetailsJobViewController.h"
#import "UIViewController+UIViewControllerExtension.h"
#import "StartJobView.h"
#import "PPJobManager.h"
#import "UIHelpers.h"
#import "PPJobModel.h"
#import "PPJobBranchModel.h"
#import "PPQuizModel.h"
#import "StartJobViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ProblemViewController.h"
#import "NSObject+NSObjectExtension.h"

@interface DetailsJobViewController ()<GMSMapViewDelegate, StartJobViewDelegate> {
    __weak StartJobView *_startJobView;
}

@end

@implementation DetailsJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self _refreshLoadData];
}

- (void)_setupNavigationBar {
    self.title = @"Carrefour Express";
    [self customBackButton];
}

- (void)_setupMapView {
        // Real
    long latitude = self.jobModel.jobBranch.branchLatitude.longValue;
    long longitude = self.jobModel.jobBranch.branchLongitude.longValue;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:10];
    self.mapView.camera = camera;
    self.mapView.delegate = self;
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    marker.position = CLLocationCoordinate2DMake(latitude, longitude);
    marker.title = self.jobModel.jobTitle;
    marker.map = self.mapView;
    self.mapView.myLocationEnabled = YES;
}

- (void)_refreshLoadData {
    if (self.jobModel) {
        [self _setupMapView];
        [kMainQueue addOperationWithBlock:^{
            self.lblName.text = self.jobModel.jobBranch.branchName;
            self.lblAddress.text = self.jobModel.jobBranch.branchAddress;
            self.tvDetail.text = self.jobModel.jobDescription;
            [self refreshValueForPreguntas:self.jobModel.jobQuizzes];
            self.lblTiempoTotal.text = [UIHelpers stringTimeFromDate:self.jobModel.jobDueDate intoDate:self.jobModel.jobApplyDate];
            self.lblValor.text = [@"$" stringByAppendingString: [NSString stringWithFormat:@"%d", self.jobModel.jobPayAmount]];
            if (_jobModel.jobImage) {
                [self.imgLogo setImageWithURL:[NSURL URLWithString:[URL_IMAGE stringByAppendingString: _jobModel.jobImage]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"]];
            }
        }];
    }
}

- (void)refreshValueForPreguntas:(NSArray*)quizArray {
    if (quizArray) {
        self.lblPreguntas.text = [NSString stringWithFormat: @"%tu", quizArray.count];
        NSString *preguntasString = @"";
        for (int i = 0; i < quizArray.count; ++i) {
            PPQuizModel *quizModel = quizArray[i];
            if (quizModel.quizContent) {
                preguntasString = [preguntasString stringByAppendingString:[[NSString stringWithFormat:@"%d. ",i+1] stringByAppendingString: quizModel.quizContent]];
            }
            if (i < quizArray.count - 1) {
                preguntasString = [preguntasString stringByAppendingString:@"\n"];
            }
        }
        self.tvPreguntas.text = preguntasString;
    } else {
        self.lblPreguntas.text = @"0";
    }
}

- (void)__showStartJobView {
    if (_startJobView == nil) {
        StartJobView *startView = [StartJobView nibView];
        [self.view addSubview:startView];
        startView.translatesAutoresizingMaskIntoConstraints = NO;
        [startView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        _startJobView = startView;
        _startJobView.delegate = self;
        _startJobView.jobLocation = [[CLLocation alloc] initWithLatitude:self.jobModel.jobBranch.branchLatitude.doubleValue longitude: self.jobModel.jobBranch.branchLongitude.doubleValue];
        _startJobView.mapView = self.mapView;
        
    }
}

- (IBAction)__actionTakeAJob:(id)sender {
    [self __showStartJobView];
    [self.viewBot setBackgroundColor:[UIColor colorWithRed:0.0/255 green:144.0/255 blue:22.0/255 alpha:1]];
}

#pragma mark: - start job view delegate
- (void)startJobview:(StartJobView *)view acionStart:(UIButton *)sender {
    [self removeJobTimerView:view];
    [view.timer invalidate];
    [self callAPIApplyJob];
    
    // [self gotoStartJob];
}

- (void)startJobview:(StartJobView *)view actionProblem:(UIButton *)sender {
    ProblemViewController *vc = [[ProblemViewController alloc] initWithNibName:[ProblemViewController identifier] bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)startJobviewDidFinishTimer:(StartJobView *)view {
    [self removeJobTimerView:view];
    [view.timer invalidate];
    [self gotoStartJob];
}

- (void)removeJobTimerView:(UIView*)view {
    [view removeFromSuperview];
    view = nil;
    [self.viewBot setBackgroundColor:[UIColor colorWithRed:200.0/255 green:213.0/255 blue:88.0/255 alpha:1]];
}
#pragma mark: - apply job
- (void)callAPIApplyJob {
    [UIHelpers showLoadingView:kWindow];
    [[PPJobManager shared] applyJobWithID: self.jobModel.jobId complition:^(BOOL success, NSString *error) {
        if (success) {
            [self gotoStartJob];
        } else if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        }
        [UIHelpers hideLoadingView];
    }];
}

- (void)gotoStartJob {
    StartJobViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[StartJobViewController identifier]];
    vc.jobModel = self.jobModel;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
