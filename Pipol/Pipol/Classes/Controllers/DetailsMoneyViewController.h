//
//  DetailsMoneyViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class LeftPaddingTextField;

@interface DetailsMoneyViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *lblMethod;
@property (nonatomic, weak) IBOutlet LeftPaddingTextField *tfMoney;
@property (nonatomic, weak) IBOutlet LeftPaddingTextField *tfEmail;
@property (nonatomic, weak) IBOutlet LeftPaddingTextField *tfRepetirEmail;
@end
