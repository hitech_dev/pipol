//
//  ResponderViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ResponderViewController.h"
#import "ResponderAnswerCell.h"
#import "ResponderQuestionCell.h"
#import "UIView+UIViewExtension.h"
#import "NSString+Help.h"
#import "PPQuizModel.h"
#import "PPJobModel.h"
#import "PPJobManager.h"
#import "UIHelpers.h"

@interface ResponderViewController ()<UITableViewDataSource, UITableViewDelegate, ResponderAnswerCellDelegate> {
    NSString *answer;
}
@end

@implementation ResponderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self _setupTableView];
}

- (void)_setupNavigationBar {
    self.title = @"Responder";
    [self customBackButton];
    [self setupBarButtonSave];
}

- (void)_setupTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[ResponderQuestionCell identifier] bundle:nil] forCellReuseIdentifier:[ResponderQuestionCell identifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[ResponderAnswerCell identifier] bundle:nil] forCellReuseIdentifier:[ResponderAnswerCell identifier]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)setQuizModel:(PPQuizModel *)quizModel {
    _quizModel = quizModel;
    answer = _quizModel.quizAnswer;
}

#pragma mark - tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int number = 0;
    if (self.quizModel) {
        if (self.quizModel.quizContent) {
            number += 1;
        }
        if (answer) {
            number += 1;
        }
    }
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {

        return [self tableView:tableView questionCellAtIndexPath:indexPath];
    } else if (indexPath.row == 1) {

        return [self tableView:tableView answerCellAtIndexPath:indexPath];
    }
    return [[UITableViewCell alloc] init];
}

- (UITableViewCell*)tableView:(UITableView *)tableView questionCellAtIndexPath:(NSIndexPath*)indexPath {
    ResponderQuestionCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResponderQuestionCell identifier] forIndexPath:indexPath];
    cell.order = self.order;
    if (self.quizModel && self.quizModel.quizContent) {
        cell.quizContent = self.quizModel.quizContent;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UITableViewCell*)tableView:(UITableView *)tableView answerCellAtIndexPath:(NSIndexPath*)indexPath {

    ResponderAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResponderAnswerCell identifier] forIndexPath:indexPath];
    cell.tvAnswer.text = answer;
    cell.delegate = self;
    cell.indexPath = indexPath;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - answer cell delegate
- (void)removeAnswerAtCell:(ResponderAnswerCell *)cell {
    answer = nil;
    [self.tableView reloadData];
}

#pragma mark - actions
- (IBAction)__actionSend:(id)sender {
    NSLog(@"%@", self.quizModel.quizAnswer);
    if (![self.tfInput.text isEmpty] && !answer) {
        answer = self.tfInput.text;
        [self.tableView beginUpdates];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
}

- (void)actionSave:(UIButton *)sender {
    [super actionSave:sender];
    [self callAPIAnswer];
}

- (void)callAPIAnswer {
    [UIHelpers showLoadingView:kWindow];
    [[PPJobManager shared] answerQuizWithQuizID:self.quizModel.quizId answer:answer complition:^(BOOL success, NSString *error) {
        [UIHelpers hideLoadingView];
        if (success) {
            self.quizModel.quizAnswer = answer;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        }
    }];
}

@end
