//
//  MapViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <PureLayout/PureLayout.h>
#import "DetailsJobViewController.h"
#import "UIViewController+UIViewControllerExtension.h"
#import "PPJobManager.h"
#import "UIHelpers.h"
#import "PPJobModel.h"
#import "PPJobBranchModel.h"

@interface MapViewController ()<GMSMapViewDelegate, CLLocationManagerDelegate> {
    GMSMapView *_mapView;
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *jobs;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self _setupMapView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self _callAPIGetJobs];
}
#pragma mark: - Setups
- (void)_setupNavigationBar {
    self.title = @"Tareas";
    [self defaultTypeNavigation];
    [self defaultRevealViewControllerOnNavigation];
}

- (void)_setupMapView {
    _mapView = [[GMSMapView alloc] initForAutoLayout];
    [self.view addSubview:_mapView];
    _mapView.translatesAutoresizingMaskIntoConstraints = NO;
    [_mapView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
    //set up location manager
    _mapView.delegate = self;
    _mapView.myLocationEnabled = YES;
    [self _setupLocationManager];	
}

- (void)_setupLocationManager {
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
        if ([CLLocationManager locationServicesEnabled]) {
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            self.locationManager.distanceFilter = 500;
            [self.locationManager startUpdatingLocation];
            
        }
    }
    //DEMO
    [self refreshJobsWithMakers: nil];
}

//refresh makers
- (void)refreshJobsWithMakers:(NSArray*) jobs{
    if (jobs) {
        for (PPJobModel *jobModel in jobs) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(jobModel.jobBranch.branchLatitude.doubleValue, jobModel.jobBranch.branchLongitude.doubleValue);
            marker.title = jobModel.jobTitle;
            marker.map = _mapView;
        }
    }
}

#pragma mark: - APIs

- (void)_callAPIGetJobs {
    [UIHelpers showLoadingView:kWindow];
    [[PPJobManager shared] getJobsByPositionLatitude:_mapView.myLocation.coordinate.latitude longitude:_mapView.myLocation.coordinate.longitude distance:100000 complition:^(NSArray *result, NSString *error) {
        if (!error) {
            [kMainQueue addOperationWithBlock:^{
                self.jobs = result;
                [self refreshJobsWithMakers:result];
            }];
        } else {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        }
        [UIHelpers hideLoadingView];
    }];
    
//    [UIHelpers showLoadingView:kWindow];
//    [[PPJobManager shared] getJobsByPositionLatitude:_mapView.myLocation.coordinate.latitude longitude:_mapView.myLocation.coordinate.longitude distance:1000 complition:^(NSArray *result, NSString *error) {
//        if (!error) {
//            [kMainQueue addOperationWithBlock:^{
//                self.jobs = result;
//                [self refreshJobsWithMakers:result];
//            }];
//        } else {
//            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
//        }
//        [UIHelpers hideLoadingView];
//    }];
    
//    [UIHelpers showLoadingView:kWindow];
//    [[PPJobManager shared] getMyJobsWithComplition:^(NSArray *result, NSString *error) {
//        if (!error) {
//            [kMainQueue addOperationWithBlock:^{
//                self.jobs = result;
//                [self refreshJobsWithMakers:result];
//            }];
//        }
//        [UIHelpers hideLoadingView];
//    }];
}

#pragma mark - Google map delegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation* location = [locations lastObject];
    GMSCameraUpdate *locationUpdate = [GMSCameraUpdate setTarget:location.coordinate zoom:16];
    [_mapView animateWithCameraUpdate:locationUpdate];
    NSLog(@"Lat: %f", location.coordinate.latitude);
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    [self gotoJobWithMaker:marker];
}

- (void)gotoJobWithMaker:(GMSMarker*)marker {
    for (PPJobModel *job in self.jobs) {
        if ([job.jobTitle isEqualToString:marker.title]) {
            [self pushDetailJobsVCWithModel:job];
            break;
        }
    }
}

- (void)pushDetailJobsVCWithModel:(PPJobModel*)model {
    DetailsJobViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[DetailsJobViewController identifier]];
    vc.jobModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
