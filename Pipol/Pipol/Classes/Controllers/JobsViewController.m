//
//  JobsViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "JobsViewController.h"
#import "JobCell.h"
#import "UIView+UIViewExtension.h"
#import "PPJobModel.h"
#import "PPJobManager.h"
#import "AppDefine.h"
#import "DummyData.h"
#import "UIHelpers.h"
#import "StartJobViewController.h"
#import "NSObject+NSObjectExtension.h"

@interface JobsViewController ()<UITableViewDataSource, UITableViewDelegate> {
    NSArray *_dataArray;
}

@end

@implementation JobsViewController

#pragma mark: - override
- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self _setupTableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self callAPIGetJobs];
}

#pragma mark: - setups
- (void)_setupNavigationBar {
    self.title = @"TAREAS";
    [self defaultTypeNavigation];
    [self defaultRevealViewControllerOnNavigation];
}

- (void)_setupTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[JobCell identifier] bundle:nil] forCellReuseIdentifier:[JobCell identifier]];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    self.tableView.tableFooterView = [[UIView alloc] init];

}

#pragma mark: - api
- (void)callAPIGetJobs {
    [UIHelpers showLoadingView:kWindow];
    [[PPJobManager shared] getMyJobsWithComplition:^(NSArray *result, NSString *error) {
        if (!error) {
            _dataArray = result;
            [kMainQueue addOperationWithBlock:^{
                self.lblTotalJobs.text = [NSString stringWithFormat:@"%tu TAREAS", _dataArray.count];
                [_tableView reloadData];
            }];
        }
        [UIHelpers hideLoadingView];
    }];
}

#pragma mark - tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_dataArray) {
        return _dataArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JobCell *cell = [tableView dequeueReusableCellWithIdentifier:[JobCell identifier] forIndexPath:indexPath];
    if (_dataArray && indexPath.row < _dataArray.count) {
        cell.jobModel = _dataArray[indexPath.row];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.separatorInset = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = NO;
    cell.layoutMargins = UIEdgeInsetsZero;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 106 * ScreenScale;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    StartJobViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[StartJobViewController identifier]];
    if (_dataArray && indexPath.row < _dataArray.count) {
        vc.jobModel = _dataArray[indexPath.row];
    }
    [self.navigationController pushViewController:vc animated:YES];
}
@end
