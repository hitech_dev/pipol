//
//  MenuViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MenuViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblUserName;
@property (nonatomic, weak) IBOutlet UIImageView *imgvAvatar;

@end
