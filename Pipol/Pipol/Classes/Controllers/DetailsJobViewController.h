//
//  DetailsJobViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <GoogleMaps/GoogleMaps.h>
@class PPJobModel;

@interface DetailsJobViewController : BaseViewController

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, weak) IBOutlet UIImageView *imgLogo;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblAddress;
@property (nonatomic, weak) IBOutlet UILabel *lblTiempoTotal;
@property (nonatomic, weak) IBOutlet UILabel *lblPreguntas;
@property (nonatomic, weak) IBOutlet UILabel *lblValor;
@property (nonatomic, weak) IBOutlet UITextView *tvDetail;
@property (nonatomic, weak) IBOutlet UITextView *tvPreguntas;
@property (nonatomic, weak) IBOutlet UIImageView *imgStick;
@property (nonatomic, weak) IBOutlet UIView *viewBot;

@property (nonatomic, strong) PPJobModel* jobModel;
@end
