//
//  RegisterViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "RegisterViewController.h"
#import "SWRevealViewController.h"
#import "NSObject+NSObjectExtension.h"
#import "LoginManager.h"
#import <AFNetworking/AFNetworking.h>
#import "PPUserModel.h"
#import "UIHelpers.h"
#import "NSString+Help.h"
#import "AppDefine.h"
#import "PPCityModel.h"
#import "MyAccountViewController.h"
#import "BaseNavigationController.h"

@interface RegisterViewController ()<UIAlertViewDelegate, UITextFieldDelegate> {
    __weak IBOutlet UITextField *tfNombre;
    __weak IBOutlet UITextField *tfDni;
    __weak IBOutlet UITextField *tfEdad;
    __weak IBOutlet UITextField *tfCiudad;
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword;
    __weak UITextField *currentTextField;
    
    UIDatePicker *datePicker;
}
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    tfDni.keyboardType = UIKeyboardTypeNumberPad;
    [self setupUI];
}

- (void)setupUI {
    datePicker = [UIHelpers datePicker];
    [datePicker addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
    [tfEdad setInputView:datePicker];
    tfEdad.delegate = self;
}

- (void)datePickerValueChanged {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    tfEdad.text = [dateFormatter stringFromDate:datePicker.date];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpNavigationBar];
    [self customBlackBackButton];
    [self setupUIWithModel: self.userModel];
}

- (void)setupUIWithModel:(PPUserModel *)model {
//    tfNombre.text = model.userName;
//    tfDni.text = [NSString stringWithFormat:@"%tu", model.userDNI];
//    tfCiudad.text = model.userCity.cityName;
//    tfEdad.text = [NSString stringWithFormat:@"%tu", model.userAge];
//    tfEmail.text = model.userEmail;
}

- (void)setUpNavigationBar {
    self.title = @"Registrarse";
    [self setupWhiteBarOfNavigation];
}

#pragma mark UITextfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == tfEdad) {
        if ([tfEdad.text isEqualToString:@""]) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
            tfEdad.text = [dateFormatter stringFromDate:datePicker.date];
        }
    }
}

#pragma mark -----
- (PPUserModel*)userModelToRegister {
    PPUserModel *model = [PPUserModel new];
    model.userName = tfNombre.text;
    model.userDNI = [tfDni.text intValue];
    model.userAddress = tfCiudad.text;
    model.userEmail = tfEmail.text;
    model.userPhone = @"123456789";
    model.userFacebookId = self.userModel.userFacebookId;
    model.userId = 0;
    return model;
}

- (void)_callAPIRegister {
    [UIHelpers showLoadingView:kWindow];
    PPUserModel *model = [self userModelToRegister];
    [[LoginManager shared] registerWithNormalUserModel:model password:tfPassword.text cityId:0 complition:^(BOOL success, NSString *error) {
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:0];
        } else {
            [kUserDefaults setObject:tfPassword.text forKey: KEY_PASSWORD_EXIST];
            [kUserDefaults synchronize];
            [self commingApp];
        }
        [UIHelpers hideLoadingView];
    }];
}

- (IBAction)__actionRegister:(id)sender {
    // Call API Register
    if ([self checkVerify]) {
        [self _callAPIRegister];
    }
}

- (void)commingApp {
    [self.view resignFirstResponder];
    MyAccountViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[MyAccountViewController identifier]];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = nav;
    [window makeKeyAndVisible];
}

- (BOOL)checkVerify {
    if ([tfNombre.text isEmpty]) {
        currentTextField = tfNombre;
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Nombre.") delegate:self tag:0];
        return NO;
    } else if ([tfEdad.text isEmpty]) {
        currentTextField = tfEdad;
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Edad.") delegate:self tag:0];
        return NO;
    } else if ([tfCiudad.text isEmpty]) {
        currentTextField = tfCiudad;
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Ciudad.") delegate:self tag:0];
        return NO;
    } else if ([tfEmail.text isEmpty]) {
        currentTextField = tfEmail;
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Email.") delegate:self tag:0];
        return NO;
    } else if (![tfEmail.text isValidEmail]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Email no es válida.") delegate:self tag:0];
        currentTextField = tfEmail;
        return NO;
    } else if ([tfPassword.text isEmpty]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Se requiere Passsword.") delegate:self tag:0];
        currentTextField = tfPassword;
        return NO;
    } else if ([tfPassword.text isCheckWhitleSpace]) {
        [UIHelpers showAlertViewErrorWithMessage:m_string(@"Passsword no es válida.") delegate:self tag:0];
        currentTextField = tfPassword;
        return NO;
    }
    return YES;
}

#pragma mark: alert delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [currentTextField becomeFirstResponder];
}
@end
