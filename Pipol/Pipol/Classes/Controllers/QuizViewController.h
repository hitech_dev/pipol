//
//  QuizViewController.h
//  Pipol
//
//  Created by HitechLtd on 4/19/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "BaseViewController.h"
@class PPQuizModel;

@interface QuizViewController : BaseViewController
@property (nonatomic, weak) IBOutlet UILabel *lblOrder;
@property (nonatomic, weak) IBOutlet UITextView *tvQuestion;

@property (nonatomic, assign) NSInteger order;
@property (nonatomic, strong) PPQuizModel *quizModel;
@end
