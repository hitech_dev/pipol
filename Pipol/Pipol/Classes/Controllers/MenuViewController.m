//
//  MenuViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "UIView+UIViewExtension.h"
#import "NSObject+NSObjectExtension.h"
#import "MyAccountViewController.h"
#import "MapViewController.h"
#import "JobsViewController.h"
#import "MoneyViewController.h"
#import "BaseNavigationController.h"
#import "LoginManager.h"
#import "PPUserManager.h"
#import "LoginViewController.h"
#import "AppDefine.h"
#import "PPUserModel.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Help.h"
#import "ImagePickerViewController.h"

#import "SWRevealViewController.h"

@interface MenuViewController ()<UITableViewDataSource, UITableViewDelegate> {
    NSArray *_dataArray;
    PPUserModel *model;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupImageView];
    [self _setupTableView];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionLogout) name:@"PIPOL_LOGOUT" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self _loadData];
}

- (void)_setupImageView {
    _imgvAvatar.layer.cornerRadius = _imgvAvatar.bounds.size.height/2;
    _imgvAvatar.clipsToBounds = YES;
}

- (void)_setupTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[MenuCell identifier] bundle:nil]  forCellReuseIdentifier:[MenuCell identifier]];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView setScrollEnabled:NO];
}

- (void)_loadData {
    _dataArray = @[@"MI CUENTA",@"VER MAPA",@"TAREAS",@"MIS CRÉDITOS",@"SALIR"];
    model = [[LoginManager shared] modelUserProfile];
    if (model) {
        self.lblUserName.text = model.userName;
    }
    if (model.userImage) {
        self.imgvAvatar.image = nil;
        [self.imgvAvatar setImageWithURL:[NSURL URLWithString: [URL_IMAGE stringByAppendingString:model.userImage]] placeholderImage:nil];
    }
}
#pragma mark - tableview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:[MenuCell identifier] forIndexPath:indexPath];
    if (_dataArray != nil && indexPath.row < _dataArray.count) {
        cell.label.text = [_dataArray objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        MyAccountViewController *myAccountVC = [self.storyboard instantiateViewControllerWithIdentifier:[MyAccountViewController identifier]];
        [self setFontViewControllerForRevealViewController:self.revealViewController withController:myAccountVC];
    } else if (indexPath.row == 1) {
        MapViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:[MapViewController identifier]];
        [self setFontViewControllerForRevealViewController:self.revealViewController withController:mapVC];
    } else if (indexPath.row == 2) {
        JobsViewController *jobsVC = [self.storyboard instantiateViewControllerWithIdentifier:[JobsViewController identifier]];
        [self setFontViewControllerForRevealViewController:self.revealViewController withController:jobsVC];
    } else if (indexPath.row == 3) {
        MoneyViewController *moneyVC = [self.storyboard instantiateViewControllerWithIdentifier:[MoneyViewController identifier]];
        [self setFontViewControllerForRevealViewController:self.revealViewController withController:moneyVC];
    } else {
        // Log out
        [self actionLogout];
    }
}

- (void)actionLogout{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    LoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[LoginViewController identifier]];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    window.rootViewController = nav;
    [[LoginManager shared] logOut];
}

- (void)setFontViewControllerForRevealViewController:(SWRevealViewController*)revealViewController withController:(UIViewController*)controller {
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:controller];
    [revealViewController setFrontViewController:nav animated:YES];
    [revealViewController revealToggleAnimated:YES];
}

#pragma mark: - actions
-(IBAction)actionPickAvatarImage:(UIButton *)sender {
//    ImagePickerViewController *imagePicker = [[ImagePickerViewController alloc] initAtController:self];
//    imagePicker.callBack = ^(UIImage *image) {
//        self.imgvAvatar.image = image;
//        self.imgvAvatar.clipsToBounds = YES;
//        NSString *imageName = [[NSString stringWithFormat:@"%tu", model.userId] stringByAppendingString:@"_UserImage_"];
//        [[PPUserManager shared] uploadImageWithImage:image imageName:[NSString stringWithFormat:@"%@.png",imageName] complition:^(BOOL success, NSString *error) {
//            
//        }];
//    };
}

- (NSString *)stringDate {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate new]];
    NSRange colonRange = [dateString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@":"] options:NSBackwardsSearch];
    return [dateString stringByReplacingCharactersInRange:colonRange withString:@""];
}
@end
