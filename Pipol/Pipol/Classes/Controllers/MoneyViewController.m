//
//  MoneyViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MoneyViewController.h"
#import "UIView+UIViewExtension.h"
#import "MoneyCell.h"
#import "MoneyHeaderSectionView.h"
#import "MoneyBalanceHeaderSectionView.h"
#import <PureLayout/PureLayout.h>
#import "DetailsMoneyViewController.h"
#import "UIViewController+UIViewControllerExtension.h"
#import "PPJobManager.h"
#import "UIHelpers.h"
#import "PPBalanceModel.h"
#import "PPJobModel.h"
#import "PPUserManager.h"

@interface MoneyViewController ()<UITableViewDataSource, UITableViewDelegate> {
    NSMutableDictionary *_dataMoney;
    __weak MoneyBalanceHeaderSectionView *_viewHeaderBalance;
    __weak MoneyHeaderSectionView *_viewHeaderRetire;
}

@end

@implementation MoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self _setupTableView];
    [self callAPIGetBalance];
}

- (void)_setupNavigationBar {
    self.title = @"MIS CRÉDITOS";
    [self defaultTypeNavigation];
    [self defaultRevealViewControllerOnNavigation];
}

- (void)_setupTableView {
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerNib:[UINib nibWithNibName:[MoneyCell identifier] bundle:nil] forCellReuseIdentifier:[MoneyCell identifier]];
    _tableView.estimatedRowHeight = 44;
    _tableView.rowHeight = 70;
    _tableView.separatorInset = UIEdgeInsetsZero;
    _tableView.tableFooterView = [[UIView alloc] init];
}

- (void)refreshDataWithArray:(NSArray*)array {
    if (array) {
        _dataMoney = [[NSMutableDictionary alloc] init];
        NSMutableArray  *saldoArray = [[NSMutableArray alloc] init];
        NSMutableArray  *retireArray = [[NSMutableArray alloc] init];
        for (PPBalanceModel *balanceModel in array) {
            if (balanceModel.balanceType == 0) {
                [saldoArray addObject:balanceModel];
            } else if (balanceModel.balanceType == 1) {
                [retireArray addObject:balanceModel];
            }
        }
        [_dataMoney setObject:saldoArray forKey:@"Saldo"];
        [_dataMoney setObject:retireArray forKey:@"Retire"];
        [self.tableView reloadData];
    }

}

#pragma mark: - API
- (void)callAPIGetBalance {
    [UIHelpers showLoadingView:kWindow];
    [[PPUserManager shared] getMyBalanceWithComplition:^(NSArray *result, NSString *error) {
        [UIHelpers hideLoadingView];
        if (error) {
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag:1];
        } else {
            [self refreshDataWithArray:result];
        }
    }];
}

#pragma mark - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_dataMoney && _dataMoney.allKeys.count > 0) {
        if (section == 0) {
            NSArray *array = [_dataMoney objectForKey:@"Saldo"];
            if (array) {
                return array.count;
            }
        } else if (section == 1) {
            NSArray *array = [_dataMoney objectForKey:@"Retire"];
            if (array) {
                return array.count;
            }
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:[MoneyCell identifier] forIndexPath:indexPath];
    if (indexPath.section == 0 && _dataMoney && [_dataMoney objectForKey:@"Saldo"]) {
        NSArray *array = [_dataMoney objectForKey:@"Saldo"];
        if (array.count > indexPath.row) {
            cell.balanceModel = array[indexPath.row];
        }
    } else if (indexPath.section == 1 && _dataMoney && [_dataMoney objectForKey:@"Retire"]) {
        NSArray *array = [_dataMoney objectForKey:@"Retire"];
        if (array.count > indexPath.row) {
            cell.balanceModel = array[indexPath.row];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.separatorInset = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = NO;
    cell.layoutMargins = UIEdgeInsetsZero;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100 * ScreenScale;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return  118 * ScreenScale;
    } else if (section == 1) {
        return 66 * ScreenScale;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        UIView *view =  [self _refreshTableViewBalanceHeader];
        if (_viewHeaderBalance) {
            NSArray *array = [_dataMoney objectForKey:@"Saldo"];
            int sumOfSaldo = 0;
            for (PPBalanceModel *model in array) {
                sumOfSaldo += model.balanceDecimal;
            }
            _viewHeaderBalance.lblValue.text = [@"$" stringByAppendingString: [NSString stringWithFormat:@"%d", sumOfSaldo] ];
        }
        return view;
    } else if (section == 1) {
        UIView *view =  [self _refreshTableViewRetireHeader];
        if (_viewHeaderRetire) {
            NSArray *array = [_dataMoney objectForKey:@"Retire"];
            int sumOfRetire = 0;
            for (PPBalanceModel *model in array) {
                sumOfRetire += model.balanceDecimal;
            }
            _viewHeaderRetire.lblValue.text = [@"-$" stringByAppendingString: [NSString stringWithFormat:@"%d", sumOfRetire]];
        }
        return view;
    }
    return nil;
}

- (UIView*)_refreshTableViewBalanceHeader {
    if (_viewHeaderBalance == nil) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, 40)];
        MoneyBalanceHeaderSectionView *headView = [MoneyBalanceHeaderSectionView nibView];
        [view addSubview:headView];
        headView.translatesAutoresizingMaskIntoConstraints = YES;
        [headView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        _viewHeaderBalance = headView;
        return view;
    }
    return _viewHeaderBalance;
}

- (UIView*)_refreshTableViewRetireHeader {
    if (_viewHeaderRetire == nil) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, 40)];
        MoneyHeaderSectionView *headView = [MoneyHeaderSectionView nibView];
        [view addSubview:headView];
        headView.translatesAutoresizingMaskIntoConstraints = YES;
        [headView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsZero];
        _viewHeaderRetire = headView;
        return view;
    }
    return _viewHeaderRetire;
}
#pragma mark - actions
- (IBAction)__actionExtractMoney:(id)sender {
    DetailsMoneyViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[DetailsMoneyViewController identifier]];
    [self.navigationController pushViewController:vc animated:YES];
}

@synthesize tableView = _tableView;
@end
