//
//  ProblemViewController.m
//  Pipol
//
//  Created by HitechLtd on 4/18/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "ProblemViewController.h"

@interface ProblemViewController () {
    __weak IBOutlet UITextView *tvAnswer;
    __weak IBOutlet UITextField *tfInput;
    NSString *answer;
}

@end

@implementation ProblemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
}

- (void)_setupNavigationBar {
    self.title = @"Problema Tarea";
    [self customBackButton];
}

#pragma mark - actions
- (IBAction)__actionSend:(id)sender {
    tvAnswer.text = tfInput.text;
}

@end
