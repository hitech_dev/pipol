//
//  MyAccountViewController.m
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import "MyAccountViewController.h"
#import "SWRevealViewController.h"
#import "ItemMyAccountView.h"
#import "PPUserManager.h"
#import "LoginManager.h"
#import "AppDefine.h"
#import "PPUserModel.h"
#import "UIHelpers.h"
#import "PPListStringPopup.h"
#import "NSObject+NSObjectExtension.h"
#import "BaseNavigationController.h"
#import "PPCityManager.h"
#import "PPCityModel.h"
#import "NSString+Help.h"
#import "UIImageView+AFNetworking.h"
#import "ImagePickerViewController.h"
#import "LoginViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MyAccountViewController ()<UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    PPUserModel *privateUserModel;
    NSInteger _indexSelectedProvince;
    NSArray *addressArray;
    NSArray *_provinceArray;
    NSArray *_cityArray;
    NSInteger _cityID;
    UIImage *_selectedCoverImage;
    NSDate *birthDate;
    UIDatePicker *datePicker;
    UIPickerView *viewPicker;
    __weak IBOutlet NSLayoutConstraint *viewContentHeight;
    __weak IBOutlet TPKeyboardAvoidingScrollView *scvScroll;
    
    NSArray *pickerData;
}

@end

@implementation MyAccountViewController

#pragma mark: - override
- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationBar];
    [self _setupCiudadView];
    [self callAPIGetProfile];
    

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    viewContentHeight.constant = self.passwordView.frame.origin.y + self.passwordView.frame.size.height + 5;
    [self.view layoutIfNeeded];

}

- (void)loadCoverImage {
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
   // NSString *coverImageString = [kUserDefaults objectForKey:KEY_USER_COVER_IMAGE];
    if (privateUserModel.userImage) {
        [self.imgView setImageWithURL:[NSURL URLWithString: [URL_IMAGE stringByAppendingString: privateUserModel.userImage]] placeholderImage:[UIImage imageNamed:@"image_placeholder.jpg"]];
    }
}
#pragma mark: - setups
- (void)_setupNavigationBar {
    self.title = @"Mi Cuenta";
    [self defaultTypeNavigation];
    [self defaultRevealViewControllerOnNavigation];
    [self setupBarButton];
    
}

- (void)setupBarButton {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"EDITAR" style:UIBarButtonItemStylePlain target:self action:@selector(actionSave:)];
}

- (void)_setupCiudadView {
    _ciudadView.layer.borderWidth = 1;
    _ciudadView.layer.borderColor = [[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1] CGColor];
    _ciudadView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
    
    datePicker = [UIHelpers datePicker];
    _tfFecha.inputView = datePicker;
    [datePicker addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
    _tfFecha.delegate = self;
    
    viewPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    viewPicker.delegate = self;
    viewPicker.dataSource = self;
    pickerData = [NSArray arrayWithObjects:@"MASCULINO",@"FEMENINO", nil];
    _tfSexo.inputView = viewPicker;
    _tfSexo.delegate = self;
}

- (void)datePickerValueChanged {
    NSDateFormatter *dateFormatter = [UIHelpers dateFormatter];
    [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    _tfFecha.text = [dateFormatter stringFromDate:datePicker.date];
}
#pragma mark - UITextfieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == _tfFecha) {
        if ([_tfFecha.text isEqualToString:@""]) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
            _tfFecha.text = [dateFormatter stringFromDate:datePicker.date];
        }
    }
    
    if (textField == _tfSexo) {
        if ([_tfSexo.text isEqualToString:@""]) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
            _tfSexo.text = pickerData[0];
        }
    }
}

#pragma mark - pickerViewDelegate
// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    _tfSexo.text = pickerData[row];
}

#pragma mark: - datas
- (void)refreshProfile:(PPUserModel*)userModel {
    if (userModel) {
        self.tfNombre.text = userModel.userName;
        self.tfApellido.text = userModel.userLastname;
        self.tfDni.text = [NSString stringWithFormat:@"%tu", (int)userModel.userDNI];
        self.tfFecha.text = [[UIHelpers dateFormatter] stringFromDate:userModel.userBirthDate];
        PPCityModel *city = userModel.userCity;
        self.lblCuidad.text = city.cityName;
        self.tfCelular.text = userModel.userPhone;
        if (userModel.userGender == 1) {
            self.tfSexo.text = @"MASCULINO";
        } else if (userModel.userGender == 2) {
            self.tfSexo.text = @"FEMENINO";
        }
        self.tfEmail.text = userModel.userEmail;
        self.tfPass.text = [kUserDefaults objectForKey:KEY_PASSWORD_EXIST];

    }
}

#pragma mark: - API
- (void)callAPIGetProfile {
    [UIHelpers showLoadingView:kWindow];
    [[PPUserManager shared] getProfileWithComplition:^(NSObject *object, NSString *error) {
        [UIHelpers hideLoadingView];
        if (!error && [object isKindOfClass:[PPUserModel class]]) {
            [kMainQueue addOperationWithBlock:^{
                [self refreshProfile:(PPUserModel*)object];
                privateUserModel = [PPUserModel new];
                privateUserModel = (PPUserModel*)object;
                [self loadCoverImage];
            }];
            
        } else {
            NSInteger tag;
            if ([error isEqualToString:@"Login incorrecto."]) {
                tag = 500;
            } else {
                tag = 1;
            }
            [UIHelpers showAlertViewErrorWithMessage:error delegate:self tag: tag];
        }
    }];
}

- (void)callAPIUploadProfile {
    [UIHelpers showLoadingView:kWindow];
    if (_selectedCoverImage) {
        NSString *imageName = [[NSString stringWithFormat:@"%tu", privateUserModel.userId] stringByAppendingString:@"_UserImage_"];
        [[PPUserManager shared] uploadImageWithImage:_selectedCoverImage imageName:[NSString stringWithFormat:@"%@.png",imageName] complition:^(BOOL success, NSString *error) {
            [[PPUserManager shared] updateProfileWithUser:[self modelOfUserProfile] cityID:_cityID oldPass:[kUserDefaults objectForKey:KEY_PASSWORD_EXIST] newPass:self.tfPass.text  complition:^(NSObject *object, NSString *error) {
                if (!error && [object isKindOfClass:[PPUserModel class]]) {
                    [kMainQueue addOperationWithBlock:^{
                        [self refreshProfile:(PPUserModel*)object];
                    }];
                }
                [UIHelpers hideLoadingView];
            }];
            [UIHelpers hideLoadingView];
        }];
    } else {
        [[PPUserManager shared] updateProfileWithUser:[self modelOfUserProfile] cityID:_cityID oldPass:[kUserDefaults objectForKey:KEY_PASSWORD_EXIST] newPass:self.tfPass.text  complition:^(NSObject *object, NSString *error) {
            if (!error && [object isKindOfClass:[PPUserModel class]]) {
                [kMainQueue addOperationWithBlock:^{
                    [self refreshProfile:(PPUserModel*)object];
                }];
            }
            [UIHelpers hideLoadingView];
        }];
    }

}

- (void)updateUserImage {
    
}

- (PPUserModel*)modelOfUserProfile {
    PPUserModel *userModel = [PPUserModel new];
    if (privateUserModel) {
        userModel = privateUserModel;
    }
    userModel.userName = self.tfNombre.text;
    userModel.userLastname = self.tfApellido.text;
    userModel.userDNI = [self.tfDni.text intValue];
    userModel.userBirthDate = [[UIHelpers dateFormatter] dateFromString:self.tfFecha.text];
    userModel.userAddress = self.lblCuidad.text;
    userModel.userPhone = self.tfCelular.text;

    userModel.userGender = [self genderValueWithModel:self.tfSexo.text];
    userModel.userEmail = self.tfEmail.text;
    userModel.userBirthDate = [[UIHelpers dateFormatter] dateFromString:self.tfFecha.text];
    userModel.userDNI = (int)self.tfDni.text.integerValue;
    return userModel;
}

- (int)genderValueWithModel:(NSString*)text {
    if ([[text uppercaseString] isEqualToString:@"MASCULINO"]) {
        return 1;
    } else if ([[text uppercaseString] isEqualToString:@"FEMENINO"]) {
        return 2;
    }
    return 0;
}
#pragma mark: - Actions
-(IBAction)actionPickCoverImage:(UIButton *)sender {
    ImagePickerViewController *imagePicker = [[ImagePickerViewController alloc] initAtController:self];
    imagePicker.callBack = ^(UIImage *image) {
        self.imgView.image = image;
        self.imgView.clipsToBounds = YES;
        _selectedCoverImage = image;
    };
}

- (void)actionSave:(UIBarButtonItem*)sender {
    UIBarButtonItem *button = sender;
    if ([button.title isEqualToString:@"GUARDAR"]) {
        button.title = @"EDITAR";
    } else {
        button.title = @"GUARDAR";
    }
    [self callAPIUploadProfile];
}

- (IBAction)actionSelectCuidad:(id)sender {
    [self presentSelectItemViewController:addressArray];
}

- (IBAction)actionSelectSexo:(id)sender {
    [self presentSelectItemViewController:addressArray];
}

- (IBAction)actionFacebook:(id)sender {
//    if(self.swFacebook.on == YES) {
//        [self askLoginFaceook];
//    }
}

- (IBAction)actionQuit:(id)sender {
    [self gotoMenu:sender];
}

#pragma mark: - select items 

- (void)presentSelectItemViewController:(NSArray*)dataArray {
    PPListStringPopup *popup = [[PPListStringPopup alloc] initWithNibName:[PPListStringPopup identifier] bundle:nil];
    popup.indexSeletedProvince = _indexSelectedProvince;
    popup.provinceArray = _provinceArray;
    popup.cityArray = _cityArray;
    popup.handler = ^(id object, NSInteger indexSelected, NSArray *provinceArray, NSArray *cityArray){
        _indexSelectedProvince = indexSelected;
        _provinceArray = provinceArray;
        _cityArray = cityArray;
        NSDictionary *diction = object;
        [kMainQueue addOperationWithBlock:^{
            self.lblCuidad.text = diction[@"name"];
        }];
        _cityID = [diction[@"id"] integerValue];
    };
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:popup];
    [self presentViewController:nav animated:true completion:nil];
}

- (NSArray*)dataForAddressPopup {
    return nil;
}

#pragma mark - alert
- (void)askLoginFaceook {
    [UIHelpers showAlertViewErrorWithMessage:@"Tienes Facebook?" cancelButton:@"Cancel" delegate:self tag:100];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            [self loginWithFacebook];
        }
    }
    if (alertView.tag == 500) {
        // Log out
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"PIPOL_LOGOUT" object:nil];
    }
}

#pragma mark: Login With Facebook
- (void)loginWithFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithPublishPermissions:nil fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            NSLog(@"Error");
        }
        else if (result.isCancelled)
        {
            NSLog(@"Cancell");
        } else {
            NSLog(@"Login Sucessfull");
            if ([FBSDKAccessToken currentAccessToken]) {
                NSLog(@"Access Token:  %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
                
            }
            if ([result.grantedPermissions containsObject:@"public_profile"])
            {
                NSLog(@"result is:%@",result);
                
                [self fetchUserInfo];
            }
        }
    }];
    
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), cover"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"resultis:%@",result);
                 if (result && [result isKindOfClass:[NSDictionary class]]) {
                     privateUserModel.userFacebookId = [result[@"id"] doubleValue];
                     NSDictionary *picDiction = result[@"picture"];
                     NSDictionary *dataDiction = picDiction[@"data"];
                     if (dataDiction[@"url"] != [NSNull null]) {
                         [kUserDefaults setObject:dataDiction[@"url"] forKey:KEY_USER_AVATAR];
                         [kUserDefaults synchronize];
                     }
                     privateUserModel.userName = result[@"name"];
                 }
             } else
             {
                 NSLog(@"Error %@",error);
             }
         }];
    }
}

@end
