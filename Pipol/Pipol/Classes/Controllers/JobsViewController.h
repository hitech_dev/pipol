//
//  JobsViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface JobsViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *lblTotalJobs;
@end
