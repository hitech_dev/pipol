//
//  MyAccountViewController.h
//  Pipol
//
//  Created by HiTechLtd on 2/23/16.
//  Copyright © 2016 HiTechLtd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ItemMyAccountView.h"

@interface MyAccountViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet ItemMyAccountView *nombreView;
@property (nonatomic, weak) IBOutlet ItemMyAccountView *dniView;
@property (nonatomic, weak) IBOutlet ItemMyAccountView *edadView;
@property (nonatomic, weak) IBOutlet UIView *ciudadView;
@property (nonatomic, weak) IBOutlet ItemMyAccountView *emailView;
@property (nonatomic, weak) IBOutlet ItemMyAccountView *passwordView;

@property (nonatomic, weak) IBOutlet UITextField *tfNombre;
@property (nonatomic, weak) IBOutlet UITextField *tfApellido;
@property (nonatomic, weak) IBOutlet UITextField *tfDni;
@property (nonatomic, weak) IBOutlet UITextField *tfFecha;
@property (nonatomic, weak) IBOutlet UILabel *lblCuidad;
@property (nonatomic, weak) IBOutlet UITextField *tfCelular;
@property (nonatomic, weak) IBOutlet UITextField *tfSexo;
@property (nonatomic, weak) IBOutlet UITextField *tfEmail;
@property (nonatomic, weak) IBOutlet UITextField *tfPass;

@end
